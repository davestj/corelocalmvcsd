-- MySQL dump 10.13  Distrib 5.6.34-79.1, for Linux (x86_64)
--
-- Host: localhost    Database: core
-- ------------------------------------------------------
-- Server version	5.6.34-79.1-56-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `_api_endpoints`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_api_endpoints` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `url_UNIQUE` (`url`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_assets`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_assets` (
  `id` int(11) NOT NULL,
  `device_type` enum('Office','Laptop','Tablet','Media Hub','TV','Phone','Printer','Speaker','Monitor','Switch','Entertainment','Gateway','Network') NOT NULL,
  `device_name` varchar(255) NOT NULL,
  `purchase_date` varchar(255) NOT NULL,
  `serial_no` text NOT NULL,
  `loan_to_email` varchar(255) NOT NULL,
  `loan_to` varchar(255) NOT NULL,
  `issue_date` varchar(255) NOT NULL,
  `current_condition` enum('New','Used','Expired','') NOT NULL,
  `returned_on` varchar(255) NOT NULL,
  `mac_address` varchar(255) NOT NULL,
  `department` enum('Office','Sales','Cx','Development','Software','Hardware') NOT NULL,
  `provider` varchar(255) NOT NULL,
  `item_value` varchar(155) NOT NULL DEFAULT '0',
  `tag` varchar(255) NOT NULL,
  `date_edited` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_assets_by_hour`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_assets_by_hour` (
  `id` int(10) NOT NULL,
  `current_assets` varchar(255) NOT NULL,
  `last_hour_assets` varchar(255) NOT NULL,
  `assets_hour` varchar(255) NOT NULL,
  `time_logged` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_dash_stats`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_dash_stats` (
  `id` int(11) NOT NULL,
  `total_assets` varchar(255) NOT NULL,
  `total_systems` varchar(255) NOT NULL,
  `total_servers` varchar(255) NOT NULL,
  `logtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `total_issues_UNIQUE` (`total_assets`),
  UNIQUE KEY `total_warnings_UNIQUE` (`total_systems`),
  UNIQUE KEY `total_outages_UNIQUE` (`total_servers`),
  KEY `id` (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_devops`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_devops` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `service_type` varchar(255) DEFAULT NULL,
  `service_link` varchar(255) DEFAULT NULL,
  `service_account` varchar(255) DEFAULT NULL,
  `service_api_key` varchar(255) DEFAULT NULL,
  `service_api_secret` varchar(255) DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `service_name_UNIQUE` (`service_name`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_finance`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_finance` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `metric` enum('Yearly','Quarterly','Monthly','OneTime') NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_issues`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_issues` (
  `id` int(10) NOT NULL,
  `current_issues` varchar(255) NOT NULL,
  `last_hour_issues` varchar(255) NOT NULL,
  `issues_hour` varchar(255) NOT NULL,
  `time_logged` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `current_issues_UNIQUE` (`current_issues`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_projects`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_projects` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category` enum('Saas','Paas','Mobile','') NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `id_UNIQUE` (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_servers`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_servers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `hostname_UNIQUE` (`hostname`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_servers_by_hour`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_servers_by_hour` (
  `id` int(10) NOT NULL,
  `current_servers` varchar(255) NOT NULL,
  `last_hour_servers` varchar(255) NOT NULL,
  `servers_hour` varchar(255) NOT NULL,
  `time_logged` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_systems`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_systems` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_systems_by_hour`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_systems_by_hour` (
  `id` int(10) NOT NULL,
  `current_systems` varchar(255) NOT NULL,
  `last_hour_systems` varchar(255) NOT NULL,
  `systems_hour` varchar(255) NOT NULL,
  `time_logged` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_test_cases`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_test_cases` (
  `id` int(11) NOT NULL,
  `test_name` varchar(255) NOT NULL,
  `test_type` enum('Selenium_IDE','Selenium BrowserStack','Unit','Human') NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `test_name_UNIQUE` (`test_name`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_users`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_users` (
  `id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `pass_word` text NOT NULL,
  `session_token` text NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_type` enum('Administrator','Developer','Management','Support') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `fname_UNIQUE` (`fname`),
  UNIQUE KEY `lname_UNIQUE` (`lname`),
  UNIQUE KEY `user_email_UNIQUE` (`user_email`),
  UNIQUE KEY `user_name_UNIQUE` (`user_name`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_users_permissions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_users_permissions` (
  `id` int(11) NOT NULL,
  `level` enum('Administrator','Developer','Sales','CustomerExperience') NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `revoked` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `user_name_UNIQUE` (`user_name`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_vendors`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_vendors` (
  `id` int(11) NOT NULL,
  `vendor_name` varchar(255) NOT NULL,
  `category` enum('Office','Hardware','Software','General Service') NOT NULL,
  `vendor_type` varchar(255) NOT NULL,
  `website_url` varchar(255) NOT NULL,
  `account_id` varchar(55) NOT NULL,
  `account_username` varchar(255) NOT NULL,
  `account_pass` text NOT NULL,
  `phone_number` varchar(155) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `account_notes` text NOT NULL,
  `date_edited` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_added` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `vendor_name_UNIQUE` (`vendor_name`),
  UNIQUE KEY `vendor_type_UNIQUE` (`vendor_type`),
  UNIQUE KEY `website_url_UNIQUE` (`website_url`),
  UNIQUE KEY `account_id_UNIQUE` (`account_id`),
  UNIQUE KEY `account_username_UNIQUE` (`account_username`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_vpn_configs`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_vpn_configs` (
  `id` int(11) NOT NULL,
  `server_id` int(11) NOT NULL,
  `config_name` varchar(255) NOT NULL,
  `connection_type` enum('tunnel','transport','passthrough','drop','reject') NOT NULL,
  `authby` enum('secret','rsasig','secret|rsasig') NOT NULL,
  `left_id` varchar(255) NOT NULL,
  `left_subnets` text NOT NULL,
  `left_sourceip` varchar(255) NOT NULL,
  `right_id` varchar(255) NOT NULL,
  `right_subnets` text NOT NULL,
  `right_sourceip` varchar(255) NOT NULL,
  `pfs` enum('Yes','No') NOT NULL,
  `ike` varchar(255) NOT NULL,
  `phase2alg` varchar(255) NOT NULL,
  `salifetime` varchar(255) NOT NULL,
  `aggrmode` enum('Yes','No') NOT NULL,
  `forceencaps` enum('Yes','No') NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `server_id_UNIQUE` (`server_id`),
  UNIQUE KEY `config_name_UNIQUE` (`config_name`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_vpn_secrets`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_vpn_secrets` (
  `id` int(11) NOT NULL,
  `config_id` int(11) NOT NULL,
  `left_pubip` varchar(255) NOT NULL,
  `right_pubip` varchar(255) NOT NULL,
  `pre_shared_key` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `config_id_UNIQUE` (`config_id`),
  UNIQUE KEY `left_pubip_UNIQUE` (`left_pubip`),
  UNIQUE KEY `right_pubip_UNIQUE` (`right_pubip`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `_vpn_servers`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_vpn_servers` (
  `id` int(11) NOT NULL,
  `dns_name` varchar(255) NOT NULL,
  `server_ip` varchar(255) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `dns_name_UNIQUE` (`dns_name`),
  UNIQUE KEY `server_ip_UNIQUE` (`server_ip`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `session_data`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session_data` (
  `session_id` varchar(32) NOT NULL DEFAULT '',
  `hash` varchar(32) NOT NULL DEFAULT '',
  `session_data` blob NOT NULL,
  `session_expire` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  UNIQUE KEY `session_id_UNIQUE` (`session_id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-13 21:55:56
