-- MySQL dump 10.13  Distrib 5.6.34-79.1, for Linux (x86_64)
--
-- Host: localhost    Database: core
-- ------------------------------------------------------
-- Server version	5.6.34-79.1-56-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `_api_endpoints`
--

LOCK TABLES `_api_endpoints` WRITE;
/*!40000 ALTER TABLE `_api_endpoints` DISABLE KEYS */;
/*!40000 ALTER TABLE `_api_endpoints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `_assets`
--

LOCK TABLES `_assets` WRITE;
/*!40000 ALTER TABLE `_assets` DISABLE KEYS */;
INSERT INTO `_assets` VALUES (1,'Laptop','DavesMacPro','03/23/2015','0101010101','it@core.local','Development','03/31/2015','New','','','Development','Apple','2500.00','0200','2015-10-07 17:23:25','2015-09-16 03:01:11'),(10,'Speaker','Sonos PLAY:3','08/01/2015','0101010101','it@core.local','Development','09/01/2015','New','','','Development','Sonoso','300.00','0197','2015-10-07 17:34:37','2015-09-16 03:27:38'),(11,'Laptop','QA Laptop','08/01/2015','0101010101','it@core.local','DevTeam','09/01/2015','Used','','','Development','','500','0186','2016-01-19 14:05:17','2015-09-17 00:53:39'),(12,'Speaker','Sonos PLAY:3','09/01/2015','0101010101','it@core.local','Development','02/01/2015','New','','','Office','','300.00','0190','2016-01-19 18:24:22','2015-09-17 21:14:37'),(13,'Speaker','Sonos CONNECT:AMP','11/01/2014','0101010101','it@core.local','Sales','09/01/2015','New','','','Sales','','500','0195','2016-01-19 18:24:45','2015-09-17 21:22:21'),(14,'Speaker','Sonos PLAY:5','11/01/2014','0101010101','it@core.local','Development','09/01/2015','New','','','Cx','Sonoso','400','0189','2016-01-19 18:25:09','2015-09-17 21:37:20'),(15,'Speaker','Lounge Sonos PLAY:3','11/01/2014','0101010101','it@core.local','Lounge','09/01/2015','New','','','Office','Sonos','300','0188','2016-01-19 18:26:08','2015-09-17 21:38:58'),(16,'TV','Samsung LED65','11/01/2014','0101010101','it@core.local','DevTeam','09/01/2015','New','','','Development','','1000','0192','2016-01-19 18:26:29','2015-09-18 02:00:41'),(17,'TV','SketchPad LG TV 1','09/01/2015','0101010101','it@core.local','Development','09/01/2015','New','','','Development','','1200','0193','2016-01-19 18:26:42','2015-09-18 02:59:57'),(18,'TV','SketchPad LG TV 2','09/01/2015','0101010101','it@core.local','Development','09/01/2015','New','','','Development','LG','1200','0194','2016-01-19 18:26:53','2015-09-18 03:00:28'),(19,'Phone','Sketch Pad Conference Bridge','11/01/2014','0101010101','it@core.local','Development','09/01/2015','New','','','Development','Polycom','25','','2016-01-19 18:27:24','2015-09-18 03:01:55'),(20,'Media Hub','Sketch Pad Apple TV','11/01/2014','0101010101','it@core.local','Development','09/01/2015','New','','','Development','Apple','75','0196','2016-01-19 18:27:08','2015-09-18 03:03:09'),(21,'Phone','Music Room Polycom Soundpoint IP 335','11/01/2014','0101010101','it@core.local','Sales','09/01/2015','Used','','','Sales','Polycom','75','','2016-01-19 18:27:32','2015-09-18 03:06:19'),(22,'Phone','Super Hero Room Polycom Soundpoint IP 335','09/01/2015','0101010101','it@core.local','Sales','09/01/2015','New','','','Sales','Polycom','55','','2016-01-19 18:27:40','2015-09-18 03:07:56'),(23,'Phone','Couch Room Polycom soundpoint IP 335','11/01/2014','0101010101','it@core.local','Sales','09/01/2015','New','','','Sales','Polycom','75','','2016-01-19 18:27:47','2015-09-18 03:08:29'),(26,'TV','Samsung LED65','11/01/2014','0101010101','it@core.local','Development','09/01/2015','New','','','Office','Samsung','1000','0169','2015-10-09 19:58:03','2015-09-18 23:19:46'),(27,'Media Hub','Apple TV','11/01/2014','0101010101','it@core.local','Main Conference Room','09/01/2015','New','','','Office','','75','0033','2016-01-19 19:34:22','2015-09-18 23:20:15'),(28,'Phone','Conference Bridge','11/01/2014','0101010101','it@core.local','Colorado Conference Room','09/01/2015','New','','','Office','Ring Central','50','','2016-01-19 18:30:57','2015-09-18 23:22:24'),(29,'TV','Vizio E601i-A3','11/01/2014','0101010101','it@core.local','Colorado Conference Room','09/01/2015','Used','','','Office','Vizio','500','0184','2015-10-09 17:24:23','2015-09-18 23:24:35'),(30,'Media Hub','Apple TV','09/01/2015','0101010101','it@core.local','Colorado Conference Room','09/01/2015','Used','','','Office','Apple','100','0185','2015-10-09 17:23:51','2015-09-18 23:25:09'),(31,'Printer','Toshiba Printer','09/01/2015','0101010101','it@core.local','Office','09/01/2015','New','','','Office','Toshiba','2000','0055','2016-01-19 19:35:31','2015-09-18 23:25:47'),(32,'Media Hub','Apple TV','09/01/2015','0101010101','it@core.local','QA','09/01/2015','New','','','Office','Apple','75','0048','2016-01-19 19:46:06','2015-09-18 23:26:50'),(33,'TV','Samsung LED65','09/01/2015','0101010101','it@core.local','Development TV','09/01/2015','Used','','','Cx','Samsung','1000','0049','2015-10-09 16:18:33','2015-09-18 23:27:42'),(34,'Phone','Mini conf room bridge','09/01/2015','0101010101','it@core.local','Office','09/01/2015','New','','','Office','Ring Central','50','','2016-01-19 18:31:01','2015-09-18 23:29:08'),(35,'Phone','Mini conf room bridge','09/01/2015','0101010101','it@core.local','Office','09/01/2015','New','','','Office','Ring Central','50','','2016-01-19 18:31:07','2015-09-18 23:29:28'),(36,'TV','Lounge Samsung LED65','09/01/2015','0101010101','it@core.local','Lounge','09/01/2015','New','','','Office','Samsung','1000','0173','2016-01-19 18:28:20','2015-09-18 23:30:43'),(37,'Media Hub','Lounge Apple TV','09/01/2015','0101010101','it@core.local','Lounge','09/01/2015','Used','','','Office','Apple','75','0172','2016-01-19 18:28:30','2015-09-18 23:31:01'),(38,'Entertainment','Playstation 4','09/01/2015','0101010101','it@core.local','Lounge','09/01/2015','New','','','Office','Sony','350','0171','2015-10-09 19:54:46','2015-09-18 23:31:30'),(39,'Switch','HP Procurve 2530-48G','09/01/2015','0101010101','it@core.local','Development','09/01/2015','New','','','Office','hp','800','0054','2016-01-19 19:34:51','2015-09-18 23:32:28'),(40,'Gateway','Sonicwall','09/01/2015','0101010101','it@core.local','Development','09/01/2015','New','','','Office','','500','0056','2016-01-19 19:37:45','2015-09-18 23:32:48'),(42,'Laptop','Winston Winpc','09/01/2015','0101010101','it@core.local','Development','09/01/2015','New','','','Development','Asus','800','0053','2016-01-19 18:28:59','2015-09-24 18:42:24'),(43,'Laptop','Dude Mac','09/01/2015','0101010101','it@core.local','Development','09/01/2015','Used','','','Development','Apple','1200','0001','2016-01-19 18:29:07','2015-09-24 18:43:07'),(44,'Laptop','Winston Mac','09/01/2015','0101010101','it@core.local','Development','09/01/2015','Used','','','Development','Apple','1200','0165','2016-01-19 18:29:14','2015-09-24 18:43:44'),(45,'Laptop','Crazy Macbook pro','09/01/2015','0101010101','it@core.local','Development','09/01/2015','Used','','','Sales','Apple','2400','00000','2016-01-19 20:44:31','2015-09-24 18:44:57'),(46,'Laptop','NO Windows PC','09/01/2015','0101010101','it@core.local','Development','09/01/2015','Used','','','Development','Asus','800','0070','2016-01-19 18:29:32','2015-09-24 18:46:07'),(47,'Laptop','Teh M@n MacbookPro','09/01/2015','0101010101','it@core.local','Development','09/01/2015','New','','','Office','Apple','2500','0163','2015-10-12 16:24:12','2015-09-24 18:47:42'),(48,'Laptop','DeadMan Macbook Pro','09/01/2015','0101010101','it@core.local','Development','01/29/2016','Used','','','Cx','Apple','1600','0167','2016-01-29 15:38:14','2015-09-24 18:48:40'),(49,'Laptop','Hookers Macbook pro','09/01/2015','0101010101','it@core.local','Development','09/01/2015','Used','','','Sales','Apple','2500','0042','2015-10-07 18:44:58','2015-09-24 18:49:21');
/*!40000 ALTER TABLE `_assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `_assets_by_hour`
--

LOCK TABLES `_assets_by_hour` WRITE;
/*!40000 ALTER TABLE `_assets_by_hour` DISABLE KEYS */;
INSERT INTO `_assets_by_hour` VALUES (1,'0','38','-38','2017-02-08 14:00:22'),(2,'0','38','-38','2017-02-08 14:00:22'),(3,'0','38','-38','2017-02-08 14:00:22'),(4,'0','38','-38','2017-02-08 14:00:22'),(5,'0','38','-38','2017-02-08 14:00:22'),(6,'0','38','-38','2017-02-08 14:00:22'),(7,'0','38','-38','2017-02-08 14:00:22'),(8,'0','38','-38','2017-02-08 14:00:22'),(9,'0','38','-38','2017-02-08 14:00:22'),(10,'0','38','-38','2017-02-08 14:00:22'),(11,'0','38','-38','2017-02-08 14:00:22');
/*!40000 ALTER TABLE `_assets_by_hour` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `_dash_stats`
--

LOCK TABLES `_dash_stats` WRITE;
/*!40000 ALTER TABLE `_dash_stats` DISABLE KEYS */;
INSERT INTO `_dash_stats` VALUES (0,'38','0','0','2017-02-08 21:00:22');
/*!40000 ALTER TABLE `_dash_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `_devops`
--

LOCK TABLES `_devops` WRITE;
/*!40000 ALTER TABLE `_devops` DISABLE KEYS */;
/*!40000 ALTER TABLE `_devops` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `_finance`
--

LOCK TABLES `_finance` WRITE;
/*!40000 ALTER TABLE `_finance` DISABLE KEYS */;
/*!40000 ALTER TABLE `_finance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `_issues`
--

LOCK TABLES `_issues` WRITE;
/*!40000 ALTER TABLE `_issues` DISABLE KEYS */;
/*!40000 ALTER TABLE `_issues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `_projects`
--

LOCK TABLES `_projects` WRITE;
/*!40000 ALTER TABLE `_projects` DISABLE KEYS */;
INSERT INTO `_projects` VALUES (1,'CoreLocalMVCSD','Paas','2017-02-09 02:49:11');
/*!40000 ALTER TABLE `_projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `_servers`
--

LOCK TABLES `_servers` WRITE;
/*!40000 ALTER TABLE `_servers` DISABLE KEYS */;
INSERT INTO `_servers` VALUES (1,'corelocal','core.local','2017-02-09 02:47:01');
/*!40000 ALTER TABLE `_servers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `_servers_by_hour`
--

LOCK TABLES `_servers_by_hour` WRITE;
/*!40000 ALTER TABLE `_servers_by_hour` DISABLE KEYS */;
INSERT INTO `_servers_by_hour` VALUES (1,'0','0','0','2017-02-08 14:00:22'),(2,'0','0','0','2017-02-08 14:00:22'),(3,'0','0','0','2017-02-08 14:00:22'),(4,'0','0','0','2017-02-08 14:00:22'),(5,'0','0','0','2017-02-08 14:00:22'),(6,'0','0','0','2017-02-08 14:00:22'),(7,'0','0','0','2017-02-08 14:00:22'),(8,'0','0','0','2017-02-08 14:00:22'),(9,'0','0','0','2017-02-08 14:00:22'),(10,'0','0','0','2017-02-08 14:00:22');
/*!40000 ALTER TABLE `_servers_by_hour` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `_systems`
--

LOCK TABLES `_systems` WRITE;
/*!40000 ALTER TABLE `_systems` DISABLE KEYS */;
INSERT INTO `_systems` VALUES (1,'Core Local Development','core.local','2017-02-09 02:47:49','2017-02-09 02:47:49');
/*!40000 ALTER TABLE `_systems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `_systems_by_hour`
--

LOCK TABLES `_systems_by_hour` WRITE;
/*!40000 ALTER TABLE `_systems_by_hour` DISABLE KEYS */;
INSERT INTO `_systems_by_hour` VALUES (1,'0','0','0','2017-02-08 14:00:22'),(2,'0','0','0','2017-02-08 14:00:22'),(3,'0','0','0','2017-02-08 14:00:22'),(4,'0','0','0','2017-02-08 14:00:22'),(5,'0','0','0','2017-02-08 14:00:22'),(6,'0','0','0','2017-02-08 14:00:22'),(7,'0','0','0','2017-02-08 14:00:22'),(8,'0','0','0','2017-02-08 14:00:22'),(9,'0','0','0','2017-02-08 14:00:22'),(10,'0','0','0','2017-02-08 14:00:22');
/*!40000 ALTER TABLE `_systems_by_hour` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `_test_cases`
--

LOCK TABLES `_test_cases` WRITE;
/*!40000 ALTER TABLE `_test_cases` DISABLE KEYS */;
/*!40000 ALTER TABLE `_test_cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `_users`
--

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;
INSERT INTO `_users` VALUES (1,'Admin','Dev','admin@core.local','admin','4cb9c8a8048fd02294477fcb1a41191a','','2017-02-14 02:51:31','Administrator');
/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `_users_permissions`
--

LOCK TABLES `_users_permissions` WRITE;
/*!40000 ALTER TABLE `_users_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `_users_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `_vendors`
--

LOCK TABLES `_vendors` WRITE;
/*!40000 ALTER TABLE `_vendors` DISABLE KEYS */;
INSERT INTO `_vendors` VALUES (1,'Ring Central','Office','VoIP Provider','https://service.ringcentral.com/','1','18775039226','G3tR0und3d','','','','2015-10-07 03:29:34','2015-10-07 03:29:34'),(2,'Cogent','Office','Internet Service Provider','http://cogentco.com','','','','','','','2015-10-07 03:31:07','2015-10-07 03:31:07');
/*!40000 ALTER TABLE `_vendors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `_vpn_configs`
--

LOCK TABLES `_vpn_configs` WRITE;
/*!40000 ALTER TABLE `_vpn_configs` DISABLE KEYS */;
/*!40000 ALTER TABLE `_vpn_configs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `_vpn_secrets`
--

LOCK TABLES `_vpn_secrets` WRITE;
/*!40000 ALTER TABLE `_vpn_secrets` DISABLE KEYS */;
/*!40000 ALTER TABLE `_vpn_secrets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `_vpn_servers`
--

LOCK TABLES `_vpn_servers` WRITE;
/*!40000 ALTER TABLE `_vpn_servers` DISABLE KEYS */;
/*!40000 ALTER TABLE `_vpn_servers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `session_data`
--

LOCK TABLES `session_data` WRITE;
/*!40000 ALTER TABLE `session_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `session_data` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-13 21:56:02
