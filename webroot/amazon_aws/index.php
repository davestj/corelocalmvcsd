<?php 
/** 
* CoreLocalMVCSD FrameWork
* GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
* 
*/ 

$viewpath = basename(__DIR__); 
require_once( dirname(__FILE__).'/../../config/config.common.php'); 
Load::model("amazon_aws"); 
Load::controller("amazon_aws"); 


/** 
* Entry view object 
*/ 
$ViewObj = new __amazon_aws("views/$viewpath","webroot/$viewpath",$_REQUEST['cache'],$_REQUEST['debug']); 
          if(isset($_REQUEST['page'])){ 
           
              $pagereq  = "__" . $_REQUEST['page']; 
              $params   = $_REQUEST; 
           
              if (method_exists($ViewObj, "$pagereq")) { 
                  $thepage = $ViewObj->$pagereq($params); 
              }else{ 
          
                  echo "page object does not exist, exiting"; 
                  exit; 
              } 
          
          
          }else{
              $ViewObj->__default();
          
          }
          


