<?php
/**
 * CoreLocalMVCSD FrameWork
 * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
 */


/**
 * Trait DBConfig
 */

trait DBConfig{

    /**
     * @return string
     */
    public static  function get_dbenv(){
        return(getenv("APP_ENVIRONMENT_TYPE"));
    }

    /**
     * @return string
     */
    public static function thehost(){
        switch(self::get_dbenv()){
            case "LOCAL":
                return ("192.168.88.20");
            break;
            case "DEVELOPMENT":
                return ("localhost");
            break;
            case "PRODUCTION":
                return ("devops.core.int");
                break;
            default:
                return ("localhost");
        }

    }

    /**
     * @return string
     */
    public static function theuser(){
        return "root";
    }


    /**
     * @return string
     */
    public static  function thepass(){
        return "";
    }

    /**
     * @return string
     */
    public static  function thedbname(){
        return "core";
    }

    /**
     * @return string
     */
    public static  function salty(){
        return('x09Cod$_3CR&iT');
    }

    /**
     * @return int
     */
    public static  function thedbport(){
        return(3306);
    }
    /**
     * @param $drivertype
     * @return int|string
     */
    public static function thedsn($drivertype){
        switch($drivertype){
            case "mysql":
                $dsn = "mysql:host=".self::thehost().";dbname=".self::thedbname().";port=".self::thedbport()."";
                return ($dsn);
                break;
            case "pgsql":
                $dsn = "pgsql:host=".self::thehost().";dbname=".self::thedbname().";port=5432";
                return ($dsn);
                break;
            case "mssql":
                $dsn = "dblib:host=".self::thehost().";dbname=".self::thedbname().";port=1433";
                return ($dsn);
                break;
            case "mongodb":
                $dsn = "NOT";
                return($dsn);
            default:
                return 3306;
        }
    }

    //legacy
    public static function Sessionconnect(){
        return(mysqli_connect(self::thehost(),self::theuser(),self::thepass(),self::thedbname(),self::thedbport()));
    }


    public static function SessionObjConnect(){
        return(new PDO(self::thedsn("mysql"),self::theuser(),self::thepass()));
     }


}


/**
 * Class DevClusterDBConfig
 */
trait DevClusterDBConfig{

    /**
     * @return string
     */
    public static function DevClusterHost(){
        return "core.local";
    }

    /**
     * @return string
     */
    public static function DevClusterUser(){
        return "";
    }


    /**
     * @return string
     */
    public static function DevClusterPass(){
        return "";
    }

    /**
     * @return string
     */
    public static function DevClusterDBName(){
        return "dev_";
    }

    /**
     * @return int
     */
    public static function DevClusterDBPort(){
        return(3306);
    }
    /**
     * @param $drivertype
     * @return int|string
     */
    public static function ClusterDSN($drivertype){
        switch($drivertype){
            case "mysql":
                $dsn = "mysql:host=".self::thehost().";dbname=".self::thedbname().";port=".self::DevClusterDBPort()."";
                return ($dsn);
                break;
            case "pgsql":
                $dsn = "pgsql:host=".self::thehost().";dbname=".self::thedbname().";port=5432";
                return ($dsn);
                break;
            case "mssql":
                $dsn = "dblib:host=".self::thehost().";dbname=".self::thedbname().";port=1433";
                return ($dsn);
                break;
            case "mongodb":
                $dsn = "NOT";
                return($dsn);
            default:
                return "mysql:host=".self::thehost().";dbname=".self::thedbname().";port=".self::DevClusterDBPort()."";
        }
    }


}



trait ProdClusterDBConfig{


    /**
     * @return string
     */
    public static function ProdClusterHost(){
        return "127.0.0.1";
    }

    /**
     * @return string
     */
    public static function ProdClusterUser(){
        return "root";
    }

    /**
     * @return string
     */
    public static function ProdClusterPass(){
        return "";
    }

    /**
     * @return string
     */
    public static function ProdClusterDBName(){
        return "core";
    }

    /**
     * @return int
     */
    public static function ProdClusterDBPort(){
        return(53306);
    }

    /**
     * @param $port
     * @return mixed
     */
    public static function ProdClusterDBTunnelPort($port){
        return($port);
    }

    public static function ProdClusterApiToken(){
        return "45645673567u356u74567567";
    }
    /**
     * @param $drivertype
     * @return int|string
     */
    public static function ClusterDSN($drivertype,$tunnel_port){
        switch($drivertype){
            case "mysql":
                $dsn = "mysql:host=".self::ProdClusterHost().";dbname=".self::ProdClusterDBName().";port=".self::ProdClusterDBPort()."";
                return ($dsn);
                break;
            case "mysql_ssh_tunnel":
                $dsn = "mysql:host=".self::ProdClusterHost().";dbname=".self::ProdClusterDBName().";port=".self::ProdClusterDBTunnelPort($tunnel_port)."";
                return ($dsn);
                break;
            case "pgsql":
                $dsn = "pgsql:host=".self::ProdClusterHost().";dbname=".self::ProdClusterDBName().";port=5432";
                return ($dsn);
                break;
            case "mssql":
                $dsn = "dblib:host=".self::ProdClusterHost().";dbname=".self::ProdClusterDBName().";port=1433";
                return ($dsn);
                break;
            case "mongodb":
                $dsn = "NOT";
                return($dsn);
            default:
                return "mysql:host=".self::ProdClusterHost().";dbname=".self::ProdClusterDBName().";port=3306";
        }
    }


}

/**
 * Trait GeneralConfig
 */
trait GeneralConfig{

    /**
     * @return string
     */
    public static function log_path(){
        return(dirname( __FILE__ ) . '/../logs/');
    }
    /**
     * @return string
     */
    public static function get_env(){
        return(getenv("APP_ENVIRONMENT_TYPE"));
    }


    public static function themeName(){
        return("vista");
    }
    /**
     * @return string
     */
    public static function _crypt_key(){
        return("blurb");
    }

    /**
     * @param $data
     * @return string
     */
    public static function safe_crypt_data($data){
        $the_key    = self::_crypt_key();
        $iv_size    = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_CBC);
        $iv         = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $txt        = mcrypt_encrypt(MCRYPT_BLOWFISH, $the_key, $data, MCRYPT_MODE_CBC, $iv);
        $txt        = $iv . $txt;
        $txt_64     = base64_encode($txt);
        return($txt_64);

    }

    /**
     * @param $data
     * @return string
     */
    public static function safe_decrypt_data($data){
        $the_key        = self::_crypt_key();
        $iv_size        = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_CBC);
        $data_dec       = base64_decode( $data);
        $iv_dec         = substr($data_dec, 0, $iv_size);
        $data_dec       = substr($data_dec, $iv_size);
        $_dec           = mcrypt_decrypt(MCRYPT_BLOWFISH, $the_key, $data_dec, MCRYPT_MODE_CBC, $iv_dec);
        return($_dec);
    }

    /**
     * @return string
     */
    public static function syshost(){
        switch(self::get_env()){
            case "LOCAL":
                return ("core.local");
                break;
            case "DEVELOPMENT":
                return ($_SERVER["HTTP_HOST"]);
                break;
            case "PRODUCTION":
                return ("core.com");
                break;
            default:
                return ("192.168.80.20");
        }

    }

    /**
     * @return string
     */
    public static function mycli(){
        return('/usr/bin/mysql');
    }

    public static function redirectToHome(){
        return(header("Location: /"));
    }

    public static function destroySession(){
        return(session_destroy());
    }

    public static function startSession(){
        return(session_start());
    }

    public static function getSessionStatus(){
        return(session_status());
    }

    public static function getSessionID(){
        return(session_id());
    }
    public static function setSessionVar($var,$value){
        return($_SESSION["$var"] = "$value");
    }

    public static function getSessionVar($varname){
        return($_SESSION["$varname"]);
    }

    public static function service_path(){
        return(__DIR__.'/../services');
    }
    /**
     * @return string
     */
    public static function model_path(){
        return(__DIR__.'/../models');
    }

    /**
     * @return string
     */
    public static function controller_path(){
        return(__DIR__.'/../controllers');
    }

    /**
     * @return string
     */
    public static function lib_path(){
        return(__DIR__.'/../lib');
    }

    /**
     * @return string
     */
    public static function vendor_path(){
        return(__DIR__.'/../vendor');
    }

    /**
     * @return string
     */
    public static function view_path(){
        return(__DIR__.'/../views');
    }

    /**
     * @return string
     */
    public static function webview_path(){
        return(__DIR__.'/../webroot');
    }

}

/**
 * Trait RedisConfig
 */
trait RedisConfig{

    public static function redis_secretpass(){
        return("CoreRedis");
    }
}
/**
 * Trait MemcacheConfig
 */
trait MemcacheConfig{

    /**
     * @return array
     */
    public static function memcache_server_list(){
        $servers = array('localhost:11211','127.0.0.1:11211');
        return ($servers);
    }

    /**
     * @return string
     */
    public static function memcache_prefix(){
        return("_CORE_");
    }

    /**
     * @return string
     */
    public static function field_prefix(){
    return("_corefield_");
    }

    /**
     * @return string
     */
    public static function table_prefix(){
        return("_coretable_");
    }


}


/**
 * Trait AwsConfig
 */
trait AwsConfig{
    /**
     * @return string
     */
    public static function cfg_aws_key(){
        return("AKIAILMVNHYGEAAWM2IA");
    }

    /**
     * @return string
     */
    public static function cfg_aws_secret(){
        return("uqZtK3DXpX6idyNPgLmLzRIufyB17lfWPjXzqe7d");
    }

}

/**
 * Trait TrelloConfig
 */
trait TrelloConfig{

    //https://api.trello.com/1/members/me/boards?
    //key=0601e49a295dcf68f65b2f6aed3c5175
    //token=6c062ab6e7107b73369e73624ad02f1c17e05c93e6589e01d5211e4da92e3fe5
    /**
     * @return string
     */
    public static  function cfg_trello_key(){
        return("0601e49a295dcf68f65b2f6aed3c5175");
    }

    /**
     * @return string
     */
    public static function cfg_trello_secret(){
        return("c78249c687e443e1282127e67213896510730d751fdb1056871072ccc012e5e5");
    }

    /**
     * @return string
     */
    public static function cfg_trello_oauth_token(){
        return("571e14ab96cfc1f830f35525a61a0a63");
    }

    /**
     * @return string
     */
    public static function cfg_trell_oauth_verifier(){
        return("6b2bc2399400bbf9ba002e80b0923cfb");
    }
}

/**
 * Trait NagiosConfig
 */
trait NagiosConfig{
    /**
     * @return string
     */
    public static  function cfg_nagius_api_url(){
        return("https://nagadmin:123456@nagios.core.local/nagios/xml/statusXML.php");
    }

}



/**
 * Trait GoogleAPIConfig
 */
trait GoogleAPIConfig{
    /**
     * @return string
     */
    public static function cfg_gapi_clientid(){
        return("281253451830-2a6hmcdec7gu4mmte6eharc27tjgf0bi.apps.googleusercontent.com");
    }

    /**
     * @return string
     */
    public static function cfg_gapi_clientsecret(){
        return("Igf86c1r2KA4z0Gbwl0CrSPM");
    }

    /**
     * @return string
     */
    public function cfg_gapi_appname(){
        return("core.local");
    }

    public static function opauth_config(){
        $config = array(
            /**
             * Path where Opauth is accessed.
             *
             * Begins and ends with /
             * eg. if Opauth is reached via http://example.org/auth/, path is '/auth/'
             * if Opauth is reached via http://auth.example.org/, path is '/'
             */
            'path' => '/auth/',

            /**
             * Uncomment if you would like to view debug messages
             */
            'debug' => true,

            /**
             * Callback URL: redirected to after authentication, successful or otherwise
             */
            'callback_url' => '{path}?page=oauth2callback',

            /**
             * Callback transport, for sending of $auth response
             *
             * 'session': Default. Works best unless callback_url is on a different domain than Opauth
             * 'post'   : Works cross-domain, but relies on availability of client-side JavaScript.
             * 'get'    : Works cross-domain, but may be limited or corrupted by browser URL length limit
             *            (eg. IE8/IE9 has 2083-char limit)
             */
            'callback_transport' => 'session',

            /**
             * A random string used for signing of $auth response.
             */
            'security_salt' => 'LDFmiilYf8Fyw5W1',

            /**
             * Higher value, better security, slower hashing;
             * Lower value, lower security, faster hashing.
             */
            //'security_iteration' => 300,

            /**
             * Time limit for valid $auth response, starting from $auth response generation to validation.
             */
            //'security_timeout' => '2 minutes',

            /**
             * Directory where Opauth strategies reside
             */
            'strategy_dir' => '{lib_dir}Strategy/',

            /**
             * Strategy
             * Refer to individual strategy's documentation on configuration requirements.
             *
             * eg.
             * 'Strategy' => array(
             *
             *   'Facebook' => array(
             *      'app_id' => 'APP ID',
             *      'app_secret' => 'APP_SECRET'
             *    ),
             *
             * )
             *
             */
            'Strategy' => array(
                // Define strategies and their respective configs here
                'Google' => array(
                    'client_id' => self::cfg_gapi_clientid(),
                    'client_secret' => self::cfg_gapi_clientsecret()
                )
            ),
        );

        return($config);
    }


}


trait ActiveDirectoryConfig{

    /**
     * @return string
     */
    public static function DomainController(){
        return("10.1.1.1");

    }

    /**
     * @return string
     */
    public static function DomainSuffix(){
        return('@ad01.core.local');
    }

    /**
     * @return string
     */
    public static function BaseDSN(){
        return("dc=ad01,dc=core,dc=local");
    }

    /**
     * @return string
     */
    public static  function BindDomainAdminUser(){
        return('Administrator');
    }

    /**
     * @return string
     */
    public static function BindDomainAdminPassW(){
        return('CHANGEME');
    }

    /**
     * @return array
     */
    public static function BindDomainAdminAccount(){
        $userdata = array('ADUserName' => self::BindDomainAdminUser(), 'ADPassWord' => self::BindDomainAdminPassW());
        return($userdata);
    }
}