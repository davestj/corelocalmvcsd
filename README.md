# CoreLocal MVCSD FrameWork
### Minimalist php framework
### Version: 1.0 beta

- Windows Setup
- Mac Setup
- Linux Setup

## Overview

- [Features](https://bitbucket.org/davestj/corelocalmvcsd/overview#markdown-header-features)
- [Windows Setup Instructions](https://bitbucket.org/davestj/corelocalmvcsd/overview#markdown-header-windows-setup-instructions)
- [Mac Setup Instructions](https://bitbucket.org/davestj/corelocalmvcsd/overview#markdown-header-mac-setup-instructions)
- [Linux Setup Instructions](https://bitbucket.org/davestj/corelocalmvcsd/overview#markdown-header-linux-setup-instructions)
- [Stack Details](https://bitbucket.org/davestj/corelocalmvcsd/overview#markdown-header-stack-details)
- [Requirements](https://bitbucket.org/davestj/corelocalmvcsd/overview#markdown-header-requirements)
- [Philosophy](https://bitbucket.org/davestj/corelocalmvcsd/overview#markdown-header-philosophy)
- [Documentation](#markdown-header-documentation) 
- [Roadmap](#markdown-header-roadmap) 
- [License »](LICENSE)  


## Features

 - Rapid Prototyping, charts & graphs
 - DevOps management for `Memcached, Redis, MySQL, MongoDB, Chef, Vagrant, Packer`
 - Health dashboard
 - Services and Daemon automation capabilities
 - API SaaS/PaaS functionality
 
## Windows Setup Instructions

To get started, checkout this repo. You will need to right click on your Git Bash desktop icon and
select "Run As Administrator", then run the following commands.

```
cd /c; git clone git@bitbucket.org:davestj/corelocalmvcsd.git; mv corelocalmvcsd core.local;
```

You will need to set the follow internal dns entry in your hosts file first before running.
```
notepad /c/Windows/System32/drivers/etc/hosts
```
Then paste the following in your hosts file
```
192.168.88.20		core.local corelocal.com
```

```
ctrl + s to save file, now exit notepad.
```
 
Once you have your hosts file in place, you can run vagrant. 
```
cd /c/core.local; vagrant up;
```
You will be prompted to choose either CentOS 6 or CentOS 7, copy verbatim the option given and enter it.

After you have your vagrant box up, you can now go to https://core.local in your browser 
and login with the default username and password of "admin" "changememan"


## Mac Setup Instructions

To get started, checkout this repo.  run the following commands.

```
cd ~/dev; git clone git@bitbucket.org:davestj/corelocalmvcsd.git; mv corelocalmvcsd core.local;
```

You will need to set the follow internal dns entry in your hosts file first before running.
```
sudo vi /etc/hosts
```
Then paste the following in your hosts file
```
192.168.88.20		core.local corelocal.com
```

```
:wq to save and exit vi
```

Once you have your hosts file in place, you can run vagrant.
```
cd ~/dev/core.local; vagrant up;
```
You will be prompted to choose either CentOS 6 or CentOS 7, copy verbatim the option given and enter it.

After you have your vagrant box up, you can now go to https://core.local in your browser 
and login with the default username and password of "admin" "changememan"


## Linux Setup Instructions

To get started, checkout this repo.  run the following commands.

```
cd ~/dev; git clone git@bitbucket.org:davestj/corelocalmvcsd.git; mv corelocalmvcsd core.local;
```

You will need to set the follow internal dns entry in your hosts file first before running.
```
sudo vi /etc/hosts
```
Then paste the following in your hosts file
```
192.168.88.20		core.local corelocal.com
```

```
:wq to save and exit vi
```

Once you have your hosts file in place, you can run vagrant.
```
cd ~/dev/core.local; vagrant up;
```
You will be prompted to choose either CentOS 6 or CentOS 7, copy verbatim the option given and enter it.

After you have your vagrant box up, you can now go to https://core.local in your browser 
and login with the default username and password of "admin" "changememan"


## Roadmap

 * General UI improvements and codeception/unit testing
 * Realtime statistics HUD improvements
 * Create "System Settings" functionality
 * Add basic monitoring system
 * Add basic release management
 * Add basic project management
 * Add Custom Ubuntu 16 x64 Ubuntu build
 * Add documentation


## Documentation

There is no documentation at the moment but will be available, see roadmap.


## Stack Details

#### Installed Packages

- Works with both CentOS 7.3.1611 and CentOS 6.8
- PHP: 7.1.1
- Nginx: 1.10.3
- Redis: 3.2.7
- Memcached: 1.4.15
- Percona XtraDB Cluster server mysql: 5.6.34
- Perl: 5.16.3
- Python: 2.7.5
- Bash: 4.2.46
- Git: 1.8.3.1
 
## Requirements

 - System you can run bash on, either windos git bash or native git client on a mac or linux desktop
 - Git >= 1.8


## Philosophy

`CoreLocal` was designed as to be a minimalist framework with the goal to rapidly deploy.


