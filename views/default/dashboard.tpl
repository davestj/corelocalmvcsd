
<!-- Tabs -->
<h2 class="demoHeaders">Core.Local Dashboard</h2>
<div id="dashboard">
    <ul>
        <li><a href="#incoming">Incoming</a></li>
        <li><a href="#scorecard">Scorecard</a></li>
        <li><a href="#reports">Reports</a></li>
    </ul>
    <div id="incoming">Incoming Items</div>
    <div id="scorecard">Scorecard metrics.</div>
    <div id="reports">Reports area.</div>
</div>