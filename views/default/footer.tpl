<script src="/theme/{$theme}/js/jquery.js"></script>
<script src="/theme/{$theme}/js/jquery.tablesorter.js"></script>
<script src="/theme/{$theme}/js/jquery.bootgrid.js"></script>
<script src="/theme/{$theme}/js/jquery.bootgrid.fa.js"></script>
<script src="/theme/{$theme}/js/jquery-ui.js"></script>
<script src="/theme/{$theme}/js/modernizr-2.8.1.js"></script>
<script>

    $("#ec2_instance_list").tablesorter();
    $("#asset_grid").tablesorter();

    $("#main_asset_grid").bootgrid({
        navigation:[2],

        formatters: {
            "link": function(column, row)
            {
                return "<a href=\"#\">" + column.id + ": " + row.id + "</a>";
            }
        },
        rowCount: [-1, 10, 25, 50, 75, 100]
    });


    var availableTags = [
        "ActionScript",
        "AppleScript",
        "Asp",
        "BASIC",
        "C",
        "C++",
        "Clojure",
        "COBOL",
        "ColdFusion",
        "Erlang",
        "Fortran",
        "Groovy",
        "Haskell",
        "Java",
        "JavaScript",
        "Lisp",
        "Perl",
        "PHP",
        "Python",
        "Ruby",
        "Scala",
        "Scheme"
    ];
    $( "#autocomplete" ).autocomplete({
        source: availableTags
    });




    $( "#button_add_asset" ).button({ icons: { primary: "ui-icon-plus" } });




    $( "#system_status" ).tabs();
    $( "#ec2_instance_detail" ).tabs();
    $( "#dashboard" ).tabs();


    $( "#dialog" ).dialog({
        autoOpen: false,
        width: 400,
        buttons: [
            {
                text: "Ok",
                click: function() {
                    $( this ).dialog( "close" );
                }
            },
            {
                text: "Cancel",
                click: function() {
                    $( this ).dialog( "close" );
                }
            }
        ]
    });

    // Link to open the dialog
    $( "#dialog-link" ).click(function( event ) {
        $( "#dialog" ).dialog( "open" );
        event.preventDefault();
    });

    // Hover states on the static widgets
    $( "#dialog-link, #icons li" ).hover(
            function() {
                $( this ).addClass( "ui-state-hover" );
            },
            function() {
                $( this ).removeClass( "ui-state-hover" );
            }
    );







    $( "#back" ).button({
        icons: {
            primary: "ui-icon-arrowreturnthick-1-w"
        },
        text: true
    });

    function goBack() {
        window.history.back();
    }




    $.ajax({
        url: "/api/?page=global_stats"
    }).then(function(data) {
        $('.asset-count').append(data.total_assets);
        $('.system-count').append(data.total_systems);
    });


    /**
     * add asset
     */
    $( "#add_asset" ).dialog({
        autoOpen: false,
        width: 600,
        height: 560,
        modal: true,
        buttons: [
            {
                text: "Add",
                click: function() {
                    $("#ADD_NEW_ASSET").submit()
                    $( this ).dialog( "close" );
                }
            },
            {
                text: "Cancel",
                click: function() {
                    $( this ).dialog( "close" );
                }
            }
        ]
    });

    // Link to open add asset dialog
    $( "#add_asset-link" ).click(function( event ) {
        $( "#add_asset" ).dialog( "open" );
        event.preventDefault();
    });


    /**
     * edit asset
     */

    {foreach from=$assets item=assetObj}


    $( "#edit_asset{$assetObj.AssetID}" ).dialog({
        autoOpen: false,
        width: 600,
        height: 560,
        modal: true,
        buttons: [
            {
                text: "Edit",
                click: function() {
                    $("#EDIT_ASSET").submit()
                    $( this ).dialog( "close" );
                }
            },
            {
                text: "Cancel",
                click: function() {
                    $( this ).dialog( "close" );
                }
            }
        ]
    });

    // Link to open edit asset dialog
    $( "#edit_asset-link{$assetObj.AssetID}" ).click(function( event ) {
        $( "#edit_asset{$assetObj.AssetID}" ).dialog( "open" );
        event.preventDefault();
    });
    //edit asset
    $( "#edit_asset-link{$assetObj.AssetID}, #icons li" ).hover(
            function() {
                $( this ).addClass( "ui-state-hover" );
            },
            function() {
                $( this ).removeClass( "ui-state-hover" );
            }
    );

    {/foreach}

    $( "#asset_datepicker" ).datepicker({
        inline: true
    });

    $( "#purch_datepicker" ).datepicker({
        inline: true
    });

    // Hover states on the static widgets
    $( "#add_asset-link, #icons li" ).hover(
            function() {
                $( this ).addClass( "ui-state-hover" );
            },
            function() {
                $( this ).removeClass( "ui-state-hover" );
            }
    );



    $('#add_asset_resultset').change(function() {
        $(this).parents('form').submit();
    });

    //load guest checkout form
    $('#ADD_NEW_ASSET').submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '/company/?page=add_new_asset',
            data: $(this).serialize(),
            success: function(data)
            {
                var result_set = $( data );
                $( "#add_asset_resultset" ).empty().append( result_set );
                //alert("form submitted" + content);
            }




        });
    });
</script>
<script type="text/javascript" src="/theme/{$theme}/js/core_local_menu.js"></script>
</body>
</html>