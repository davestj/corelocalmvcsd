<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>core.local</title>
    <link href="/theme/{$theme}/css/jquery-ui.structure.css" rel="stylesheet">
    <link href="/theme/{$theme}/css/jquery-ui.css" rel="stylesheet">
    <link href="/theme/{$theme}/css/jquery-ui.theme.css" rel="stylesheet">

    <style>
        @-webkit-viewport { width: device-width; }
        @-moz-viewport { width: device-width; }
        @-ms-viewport { width: device-width; }
        @-o-viewport { width: device-width; }
        @viewport { width: device-width; }
        body{
            font-family: "Trebuchet MS", sans-serif;
            margin: 50px;
        }
        html, body {
            padding: 0;
            margin: 0;
            height: 100%;
        }
        .demoHeaders {
            margin-top: 2em;
        }
        #dialog-link {
            padding: .4em 1em .4em 20px;
            text-decoration: none;
            position: relative;
        }

        #dialog-link span.ui-icon {
            margin: 0 5px 0 0;
            position: absolute;
            left: .2em;
            top: 50%;
            margin-top: -8px;
        }
        #add_asset-link span.ui-icon {
                     margin: 0 5px 0 0;
                     position: absolute;
                     left: .2em;
                     top: 50%;
                     margin-top: -8px;
        }

        #edit_asset-link span.ui-icon {
            margin: 0 5px 0 0;
            position: absolute;
            left: .2em;
            top: 50%;
            margin-top: -8px;
        }
        #icons {
            margin: 0;
            padding: 0;
        }
        #icons li {
            margin: 2px;
            position: relative;
            padding: 4px 0;
            cursor: pointer;
            float: left;
            list-style: none;
        }
        #icons span.ui-icon {
            float: left;
            margin: 0 4px;
        }
        .fakewindowcontain .ui-widget-overlay {
            position: absolute;
        }
        select {
            width: 200px;
        }
        html[xmlns] #menu-bar {
            display: block;
        }
        * html #menu-bar {
            height: 1%;
        }
        .wrapper {
            margin: 0 auto;

            height: auto !important;
        }
        .clearfix:before,
        .clearfix:after {
            content: " ";
            display: table;
        }

        .clearfix:after {
            clear: both;
        }

        .clearfix {
            *zoom: 1;
        }
        .data_info{
            position: absolute;
            top: 0px !important;
            margin: 0px 0px 0px 0px !important;
            padding-top: 6px !important;
            padding-left: 2px !important;
            right: 0px;
            width: 420px;
            height: 90px;
            background: #424242;
            color: #FFFFFF;
            border: dashed;
            line-height: 4px !important;
            font: 85% "Verdana", sans-serif;
        }
        /* tables */
        table.tablesorter {
            font-family:arial;
            background-color: #CDCDCD;
            margin:10px 0pt 15px;
            font-size: 8pt;
            width: 100%;
            text-align: left;
        }
        table.tablesorter thead tr th, table.tablesorter tfoot tr th {
            background-color: #ccc;
            border: 1px dashed #ddd;
            font-size: 8pt;
            padding: 4px;
        }
        table.tablesorter thead tr .header {
            background-image: url(/theme/{$theme}/images/bg.gif);
            background-repeat: no-repeat;
            background-position: center right;
            cursor: pointer;
        }
        table.tablesorter tbody td {
            color: #3D3D3D;
            padding: 4px;
            background-color: #FFF;
            vertical-align: top;
        }
        table.tablesorter tbody tr.odd td {
            background-color:#F0F0F6;
        }
        table.tablesorter thead tr .headerSortUp {
            background-image: url(/theme/{$theme}/images/asc.gif);
        }
        table.tablesorter thead tr .headerSortDown {
            background-image: url(/theme/{$theme}/images/desc.gif);
        }
        table.tablesorter thead tr .headerSortDown, table.tablesorter thead tr .headerSortUp {
            background-color: #aed119;
            color: #ffffff;
        }
        .host-count{
            padding: 4px 0px 0px;
            line-height: 2px;
        }
        .service-count{
            line-height: 2px;
        }
        .back-ui-button {
            border-radius: 10px;
        }

        iframe:focus {
            outline: none;
        }

        iframe[seamless] {
            display: block;
        }
    </style>
    <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link rel="stylesheet" href="/theme/{$theme}/css/core_local_menu.css" type="text/css" />

</head>
<body id="body">

<header class="wrapper clearfix">
    <div class="data_info">{$dateset}<br>
        <p class="asset-count">Assets: </p>
        <p class="system-count">Systems: </p>
    </div>
    <article><a href="/"><img src="/theme/{$theme}/images/core.local-3d-logo_transparent.png" width="200" height="98" border="0"></a></article>

    <nav>
        <div id="mbmcpebul_wrapper">
            <ul id="mbmcpebul_table" class="mbmcpebul_menulist css_menu">
                <li class="first_button"><div class="buttonbg gradient_button gradient40"><div class="arrow"><a>Home</a></div></div>
                    <ul>
                        <li><a href="/?page=dashboard" title="">Dashboard</a></li>
                        <li><a href="/?page=logout" title="">Logout</a></li>
                    </ul></li>
                <li><div class="buttonbg gradient_button gradient40" style="width: 105px;"><div class="arrow"><a>Company</a></div></div>
                    <ul>
                        <li><a href="/company/?page=assets" title="">Assets</a></li>
                        <li><a href="/company/?page=vendors" title="">Vendors</a></li>
                        <li><a href="/company/?page=finance" title="">Finance</a></li>
                    </ul></li>
                <li><div class="buttonbg gradient_button gradient40" style="width: 93px;"><div class="arrow"><a>DevOps</a></div></div>
                    <ul>
                        <li><a href="/devops/?page=apimanagement" title="">API Management</a></li>
                        <li><a href="/amazon_aws/?page=default" title="">AWS Cloud</a></li>
                        <li><a class="with_arrow" title="">Build Tools</a>
                            <ul class="img_16">
                                <li><a href="/devops/?page=jenkins" class="with_img_16" title=""><img src="/theme/{$theme}/css/images/mbico_mbmcp_1.png" alt="" />Jenkins</a></li>
                            </ul></li>
                        <li><a class="with_arrow" title="">Database</a>
                            <ul>
                                <li><a target="_blank" href="https://admin:CoreLocal@core.local:8092/index.php" title="">Memcache Admin</a></li>
                                <li><a target="_blank" href="https://admin:CoreLocal@core.local:8090/index.php" title="">Redis Admin</a></li>
                                <li><a target="_blank" href="/phpmyadmin/" title="">phpMyAdmin</a></li>
                                <li><a target="_blank" href="/pgadmin/" title="">pg Admin</a></li>
                                <li><a target="_blank" href="/mongoAdmin/" title="">mongoAdmin</a></li>
                            </ul></li>
                        <li><a href="/devops/?page=monitoring" title="">Monitoring</a></li>
                        <li><a href="/devops/?page=projects" title="">Projects</a></li>
                        <li><a href="/devops/?page=servers" title="">Servers</a></li>
                    </ul></li>
                <li><div class="buttonbg gradient_button gradient40" style="width: 102px;"><div class="arrow"><a>Releases</a></div></div>
                    <ul>
                        <li><a href="/releases/?page=dashboard" title="">Dashboard</a></li>
                    </ul></li>
                <li><div class="buttonbg gradient_button gradient40" style="width: 129px;"><div class="arrow"><a>Development</a></div></div>
                    <ul>
                        <li><a href="/development/?page=scm" title="">SCM</a></li>
                        <li><a href="/development/?page=staging" title="">Staging</a></li>
                    </ul></li>
                <li><div class="buttonbg gradient_button gradient40" style="width: 113px;"><div class="arrow"><a>QA Testing</a></div></div>
                    <ul>
                        <li><a href="/qa/?page=smoketests" title="">Smoke Tests</a></li>
                        <li><a href="/qa/?page=selenium" title="">Selenium</a></li>
                        <li><a href="/qa/?page=unittests" title="">Unit Tests</a></li>
                    </ul></li>
                <li><div class="buttonbg gradient_button gradient40" style="width: 135px;"><div class="arrow"><a>External Links</a></div></div>
                    <ul>
                        <li><a class="with_arrow" title="">Database</a>
                            <ul>
                                <li><a href="/dbase_myadmin/" title="">phpMyAdmin</a></li>
                            </ul></li>
                        <li><a class="with_arrow" title="">Development</a>
                            <ul>
                                <li><a href="https://bitbucket.org" target="_blank" title="">Stash/Bitbucket</a></li>
                                <li><a href="/jenkins/" title="">Jenkins</a></li>
                                <li><a href="https://www.browserstack.com/" target="_blank" title="">BrowserStack</a></li>
                                <li><a href="https://login.newrelic.com" target="_blank" title="">New Relic</a></li>
                            </ul></li>
                    </ul></li>
                <li class="last_button"><div class="buttonbg gradient_button gradient40"><div class="arrow"><a>About</a></div></div>
                    <ul>
                        <li><a href="/?page=info" title="">Debug</a></li>
                        <li><a href="/?page=release_data" title="">Release Information</a></li>
                    </ul></li>
            </ul>
        </div>

    </nav>
</header>

