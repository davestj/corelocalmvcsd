
<!-- Tabs -->
<h2 class="">System Status</h2>
<div id="system_status">
    <ul>
        <li><a href="#servers"><span>Monitored Servers</span></a></li>
        <li><a href="#setup_data"><span>Setup Data</span></a></li>
        <li><a href="#session">Session Data</a></li>
        <li><a href="#debug">Debug</a></li>
    </ul>
    <div id="servers"><iframe scrolling="yes" frameborder="0" src="/devops/?page=tab_monitoring" width="100%" height="600px" border="0"></iframe> </div>
    <div id="setup_data"><iframe scrolling="yes" frameborder="0" src="/api/?page=setup_complete" width="100%" height="600px" border="0"></iframe> </div>
    <div id="session">{$dateset} <br> <iframe scrolling="yes" frameborder="0" src="/api/?page=session_data" width="100%" height="600px" border="0"></iframe></div>
    <div id="debug"><iframe scrolling="yes" frameborder="0" src="/?page=info" width="100%" height="600px" border="0"></iframe></div>