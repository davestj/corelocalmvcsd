<h3>EC2 Instance Details</h3>
<button id="back" onclick="goBack()" class="back-ui-button">Back</button>
<br><br>
{foreach from=$ec2_intance item=data}
{/foreach}


<!-- Tabs -->
<div id="ec2_instance_detail">
    <ul>
        <li><a href="#info">Instance Detail</a></li>
        <li><a href="#network">Network</a></li>
        <li><a href="#storage">Storage</a></li>
        <li><a href="#sys_reports">System Reports</a></li>
    </ul>
    <div id="info">
        <table>
        <tr>
            <td>Name</td>
            <td>{$data.InstanceName}</td>
        </tr>
        <tr>
            <td>Current state</td>
            <td>{$data.State}</td>
        </tr>
        <tr>
            <td>Monitoring</td>
            <td>{$data.MonitoringState}</td>
        </tr>
        <tr>
            <td>Instance type</td>
            <td>{$data.InstanceType}</td>
        </tr>
            <tr>
                <td>Virtualization type</td>
                <td>{$data.VirtType}</td>
            </tr>
            <tr>
                <td>Architecture</td>
                <td>{$data.Arch}</td>
            </tr>
        </table>
    </div>
    <div id="network">
        <table>
            </tr>
                <td>MAC Address</td>
                <td>{$data.NetworkMAC}</td>
            </tr>
            <tr>
                <td>Private IP</td>
                <td>{$data.PrivateIP}</td>
            </tr>
            <tr>
                <td>Private DNS</td>
                <td>{$data.PrivateDnsName}</td>
            </tr>
            <tr>
                <td>Public IP</td>
                <td>{$data.PublicIP}</td>
            </tr>
            <tr>
                <td>Public DNS</td>
                <td>{$data.PublicDNS}</td>
            </tr>
        </table>
    </div>
    <div id="storage">Storage  Info
    <table>
        </tr>
        <td>Root Device Type</td>
        <td>{$data.RootDevType}</td>
        </tr>
        <tr>
            <td>Root Device</td>
            <td>{$data.RootDevice}</td>
        </tr>

        {foreach from=$data.DeviceData item=devdata}
            <tr>
                <td>#{$devdata.DeviceCount}. Device Name</td>
                <td>{$devdata.DeviceName}</td>
            </tr>
        {/foreach}

    </table>
    </div>
    <div id="sys_reports"><iframe frameborder="0" src="/api/?page=stats" width="100%" height="600px" border="0"></iframe></div>
</div>


