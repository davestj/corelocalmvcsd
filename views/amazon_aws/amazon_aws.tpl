<h3>EC2 Instances</h3>
<table id="ec2_instance_list" class="tablesorter">
    <thead>
    <tr>
        <th>Action</th>
        <th>No.#</th>
        <th>Name</th>
        <th>State</th>
        <th>Monitoring</th>
        <th>Type</th>
        <th>NetworkMAC</th>
        <th>PrivateIP</th>
    </tr>
    </thead>
    <tbody>

    {foreach from=$ec2_intances key=instanceID item=data}
        <tr>
            <td><a href="?page=aws_ec2_details&id={$data.InstanceID}">View Details</a></td>
            <td>{$instanceID}</td>
            <td>{$data.InstanceName}</td>
            <td>{$data.State}</td>
            <td>{$data.MonitoringState}</td>
            <td>{$data.InstanceType}</td>
            <td>{$data.NetworkMAC}</td>
            <td>{$data.PrivateIP}</td>
        </tr>
    {/foreach}


    </tbody>
</table>