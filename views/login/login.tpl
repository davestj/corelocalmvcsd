<!DOCTYPE html>
    <!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
    <!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
    <!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
    <!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>core.local</title>
    <link href="/theme/{$theme}/css/jquery-ui.css" rel="stylesheet">


    <style>
        body{
            font-family: "Trebuchet MS", sans-serif;
            margin: 50px;
            line-height: 1;
        }
        html, body {
            padding: 0;
            margin: 0;
            width: 1280;
            height: 720;
        }
        html{

            background-image: url(/theme/{$theme}/images/1920x1080-core-local-login.png);
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            background-repeat: no-repeat;
            background-attachment:fixed;
        }

        .fakewindowcontain .ui-widget-overlay {
            position: absolute;
        }
        select {
            width: 200px;
        }
        #forgot_pass-link {
            padding: .4em 1em .4em 20px;
            text-decoration: none;
            position: relative;
        }
        #forgot_pass-link span.ui-icon {
            margin: 0 5px 0 0;
            position: absolute;
            left: .2em;
            top: 50%;
            margin-top: -8px;
        }

        .wrapper {
            margin: 0 auto;
        }
        .clearfix:before,
        .clearfix:after {
            content: " ";
            display: table;
        }

        .clearfix:after {
            clear: both;
        }

        .clearfix {
            *zoom: 1;
        }
        .login{
            width: 100%;
            padding: 25px 5px 40px auto;
            background-image: url("/theme/{$theme}/images/1920x1080-core-local-login.png");
            background-size: cover;
            background-repeat: no-repeat;
            background-attachment:fixed;
        }
        .loginbox{
            position: relative;
            top: 160px;
            left: 200px;
        }
    </style>
        <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <link rel="stylesheet" href="/theme/{$theme}/css/login_style.css">
</head>
<body id="body">
<div class="login">
<div class="clearfix wrapper loginbox">

    <form method="post" class="login" id="login_auth">
        <p>
            <label for="login">User Name:</label>
            <input type="text" name="UserName" id="login" value="">
        </p>

        <p>
            <label for="password">Password:</label>
            <input type="password" name="Password"" id="password" value="">
        </p>

        <p class="login-submit">
            <button type="submit" class="login-button" name="Login" >Login</button>
        </p>

        <p class="forgot-password"><a href="#" id="forgot_pass-link">
                <span class="ui-icon ui-icon-newwin"></span>Forgot your password?</a>
        </p>
    </form>

</div>


</div>
<!-- ui-dialog -->
<div id="forgot_pass" title="Forgot Password">
    <p>Enter your email address to retrieve forgot or lost password.</p>
</div>
<script src="/theme/{$theme}/js/jquery.js"></script>
<script src="/theme/{$theme}/js/jquery-ui.js"></script>
<script>
    function custom_alert(output_msg, title_msg)
    {
        if (!title_msg)
            title_msg = 'Alert';

        if (!output_msg)
            output_msg = 'No Message to Display.';

        $("<div></div>").html(output_msg).dialog({
            title: title_msg,
            resizable: false,
            modal: true,
            buttons: {
                "Ok": function()
                {
                    $( this ).dialog( "close" );
                }
            }
        });
    }

// url: '/auth/?page=active_directory_auth',
    $('#login_auth').submit(function(e) {
        e.preventDefault();
            $.ajax({
                type: "POST",
                url: '/auth/?page=authenticate',
                data: $(this).serialize(),
                success: function(data)
                {

                    if (data === "OK"){
                        window.location = '/page=?dashboard';

                    } else if (data === "EMPTY") {
                        custom_alert("Username and password left out<br>please try again.", "Authentication Error");
                    }else if( data === "AUTH_FAIL" ){
                        custom_alert("Bad username and password entered<br>please try again.", "Authentication Error");
                    }
                }
            });

    });

    $( "#forgot_pass" ).dialog({
        autoOpen: false,
        width: 400,
        buttons: [
            {
                text: "Ok",
                click: function() {
                    $( this ).dialog( "close" );
                }
            },
            {
                text: "Cancel",
                click: function() {
                    $( this ).dialog( "close" );
                }
            }
        ]
    });

    // Link to open the dialog
    $( "#forgot_pass-link" ).click(function( event ) {
        $( "#forgot_pass" ).dialog( "open" );
        event.preventDefault();
    });

    // Hover states on the static widgets
    $( "#forgot_pass-link, #icons li" ).hover(
            function() {
                $( this ).addClass( "ui-state-hover" );
            },
            function() {
                $( this ).removeClass( "ui-state-hover" );
            }
    );


</script>
</body>
</html>

