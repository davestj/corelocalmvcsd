<style type="text/css">
    .add_assetLayout
    {
        background-color: #f3f3f3;
        padding: 1px;
        width: 98%;
        border: 0px;
    }

    .add_assetLayout label, .add_assetLayout input, .add_assetLayout textarea
    {
        display: block;
        width: 160px;
        float: left;
        margin-bottom: 10px;
    }

    .add_assetLayout label
    {
        text-align: right;
        padding-right: 20px;
    }

    br
    {
        clear: left;
    }

    ._assetLayout
    {
        background-color: #f3f3f3;
        padding: 1px;
        width: 98%;
        border: 0px;
    }

    ._assetLayout label, ._assetLayout input, ._assetLayout textarea
    {
        display: block;
        width: 160px;
        float: left;
        margin-bottom: 10px;
    }

    ._assetLayout label
    {
        text-align: right;
        padding-right: 20px;
    }
</style>

<br>
<a href="#" id="add_asset-link""><button id="button_add_asset">Add Asset</button></a>
<div id="add_asset_resultset"></div>

<table id="asset_grid" class="tablesorter table-condensed table-hover table-striped">
    <thead>
    <tr>
        <th>Action</th>
        <th>ID</th>
        <th data-column-id="d_type"  data-filterable="true">Device Type</th>
        <th data-column-id="d_name" data-filterable="true" data-order="asc">Device Name</th>
    </tr>
    </thead>
    <tbody>

    {foreach from=$assets item=assetObj}
    <tr>
        <td><a id="edit_asset-link{$assetObj.AssetID}" href="#">Edit</a></td>
        <td> {$assetObj.AssetID}</td>
        <td> {$assetObj.DeviceType}</td>
        <td> {$assetObj.DeviceName}</td>
    </tr>
    {/foreach}
    </tbody>
</table>



{foreach from=$assets item=assetObj}
<div id="edit_asset{$assetObj.AssetID}" title="Editing Asset ID# {$assetObj.AssetID}">
    Editing asset {$assetObj.DeviceName}
    <form id="EDIT_NEW_ASSET{$assetObj.AssetID}" name="edit_new_asset{$assetObj.AssetID}" method="POST" action="?page=edit_asset&assetID={$assetObj.AssetID}">
        <div class="_assetLayout">
            <label>Device Type</label>
            <input id="device_type" name="device_type" value=" {$assetObj.DeviceType}"><br>

            <label>Device Name</label>
            <input id="device_name" name="device_name" value="{$assetObj.DeviceName}"><br>

            <label>Purchase Date</label>
            <input id="purch_datepicker" name="purch_date"><br>

            <label>Serial No.</label>
            <input id="serial_num" name="serial_num"><br>

            <label>Loaned to Email</label>
            <input id="loaned_to_email" name="loaned_to_email"><br>

            <label>Loaned To</label>
            <input id="loaned_to" name="loaned_to"><br>

            <label>Issue Date</label>
            <input id="issue_datepicker" name="issue_date"><br>


            <label>Current Condition</label>
            <select name="current_condition">
                <option>New</option>
                <option>Used</option>
                <option>Expired</option>
            </select><br>

            <label>MAC Address</label>
            <input id="mac_address" name="mac_address"><br>

            <label>Category</label>
            <select id="category" name="asset_category">
                <option>Office</option>
                <option>Sales</option>
                <option>CX</option>
                <option>Development</option>
                <option>Software</option>
                <option>Hardware</option>
            </select><br>

            <label>Provider</label>
            <input id="provider" name="provider"><br>

            <label>Item Value</label>
            <input id="item_value" name="item_value"><br>

            <label>Tag #</label>
            <input id="tag_id" name="tag_id"><br>
    </form>
</div>
{/foreach}


<div id="add_asset" title="Adding New Asset">

    <form id="ADD_NEW_ASSET" name="add_new_asset" method="POST" action="?page=add_new_asset">

    <div class="add_assetLayout">
        <label>Device Type</label>
        <input id="device_type" name="device_type"><br>

        <label>Device Name</label>
        <input id="device_name" name="device_name"><br>

        <label>Purchase Date</label>
        <input id="purch_datepicker" name="purch_date"><br>

        <label>Serial No.</label>
        <input id="serial_num" name="serial_num"><br>

        <label>Loaned to Email</label>
        <input id="loaned_to_email" name="loaned_to_email"><br>

        <label>Loaned To</label>
        <input id="loaned_to" name="loaned_to"><br>

        <label>Issue Date</label>
        <input id="issue_datepicker" name="issue_date"><br>


        <label>Current Condition</label>
        <select name="current_condition">
            <option>New</option>
            <option>Used</option>
            <option>Expired</option>
        </select><br>

        <label>MAC Address</label>
        <input id="mac_address" name="mac_address"><br>

        <label>Category</label>
        <select id="category" name="asset_category">
            <option>Office</option>
            <option>Sales</option>
            <option>CX</option>
            <option>Development</option>
            <option>Software</option>
            <option>Hardware</option>
        </select><br>

        <label>Provider</label>
        <input id="provider" name="provider"><br>

        <label>Item Value</label>
        <input id="item_value" name="item_value"><br>

        <label>Tag #</label>
        <input id="tag_id" name="tag_id"><br>

    </form>

    </div>
</div>
