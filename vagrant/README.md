#CoreLocal MVCSD FrameWork
###Vagrant setup overview

From the core.local directory, run `vagrant up`. You will be asked the following question
`What OS are you installing [ CentOS6 | CentOS7 | default ] ?:`
You can just hit enter for the default CentOS 6, if you need to deploy and develop
on CentOS7, simply enter in `CentOS7` at the prompt.

All Major OS are supported deploying on CentOS 6 x64 and CentOS 7 x64 with support for Ubuntu 16 x64 coming
