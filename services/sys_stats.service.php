<?php
/**
 * CoreLocalMVCSD FrameWork
 * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
 *
 * Service sys_statsService
 *
 */

class sys_statsService  extends sys_statsModel
{


    /**
     * @var
     */
    public $dateset;

    /**
     * @var string
     */
    public $xml    = '';

    /**
     * @var array
     */
    public $apidata = '';

    /**
     * @var
     */
    public $clusterObj;

    /**
     * @var array
     */
    public $error = '';
    /**
     * @var
     */
    public $sqlD;

    /**
     * @param $dsn
     * @param string $user
     * @param string $passwd
     */
    public function _construct($dsn, $user = "", $passwd = ""){
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        try {
            parent::__construct($dsn, $user, $passwd, $options);

        } catch (PDOException $e) {
            $this->error = $e->getMessage();
        }


    }

    /**
     * @return string
     */
    public function get_all_assets(){
        $total_assets = parent::get_total_assets();
        return($total_assets);
    }

    /**
     * @return string
     */
    public function get_all_systems(){
        $total_systems = parent::get_total_systems();
        return($total_systems);
    }

    /**
     * @return string
     */
    public function get_all_servers(){
        $total_servers = parent::get_total_servers();
        return($total_servers);
    }


    public function update_dash_status($total_assets,$total_systems,$total_servers){

        $sql = "INSERT INTO `_dash_stats` (`total_assets`, `total_systems`, `total_servers`)
        VALUES (
        \"$total_assets\",
        \"$total_systems\",
        \"$total_servers\");";

        try {
            if(parent::query_proc("$sql")){
                return("UPDATED");
            }else {
                return("FAILED");
            }

        }catch(PDOException $ex){
            return( "ERROR: ". $ex->getMessage() ."\n");

        }
    }

    /**
     * @return string
     */
    public function calculate_assets_by_hour(){
        $sql = "SELECT total_assets,logtime FROM _dash_stats where logtime >= DATE_SUB(NOW(), INTERVAL 2 HOUR)";
        $data = parent::query_obj("$sql");

        $lasthour = (int) str_replace(',','',$data[0]->total_assets);
        $curhour = (int) str_replace(',','',$data[1]->total_assets);

        $assets_lasthour = $curhour - $lasthour;


            $current_hour                   = $curhour;
            $current_hour_timestamp         = $data[0]->logtime;
            $last_hour                      = $lasthour;
            $last_hour_timestamp            = $data[1]->logtime;
            $counted_last_hour_assets       = $assets_lasthour;


        $sql = "INSERT INTO `_assets_by_hour` (`current_assets`, `last_hour_assets`, `assets_hour`, `time_logged`)
        VALUES (
        \"$current_hour\",
        \"$last_hour\",
        \"$counted_last_hour_assets\",
        \"$current_hour_timestamp\");";

        if(parent::query_proc("$sql")){
          $msgtxt = "[$current_hour_timestamp] CURRENT HOUR assets:". $data[0]->total_assets ."\n
          [$last_hour_timestamp] LAST HOUR ROUNDS " .$data[1]->total_assets."\n
          Total Assets last hour: $counted_last_hour_assets\n ";
        }

        return($msgtxt);

    }

    /**
     * @return string
     */
    public function calculate_systems_by_hour(){
        $sql = "SELECT total_systems,logtime FROM _dash_stats where logtime >= DATE_SUB(NOW(), INTERVAL 2 HOUR)";
        $data = parent::query_obj("$sql");

        $lasthour = (int) str_replace(',','',$data[0]->total_systems);
        $curhour = (int) str_replace(',','',$data[1]->total_systems);

        $systems_lasthour = $curhour - $lasthour;


        $current_hour                   = $curhour;
        $current_hour_timestamp         = $data[0]->logtime;
        $last_hour                      = $lasthour;
        $last_hour_timestamp            = $data[1]->logtime;
        $counted_last_hour_systems      =  $systems_lasthour;


        $sql = "INSERT INTO `_systems_by_hour` (`current_systems`, `last_hour_systems`, `systems_hour`, `time_logged`)
        VALUES (
        \"$current_hour\",
        \"$last_hour\",
        \"$counted_last_hour_systems\",
        \"$current_hour_timestamp\");";

        if(parent::query_proc("$sql")){
            $msgtxt = "[$current_hour_timestamp] CURRENT HOUR systems:". $data[0]->total_systems ."\n
          [$last_hour_timestamp] LAST HOUR systems " .$data[1]->total_systems."\n
          Total systems last hour: $counted_last_hour_systems\n ";
        }

        return($msgtxt);

    }


    public function calculate_servers_by_hour(){
        $sql = "SELECT total_servers,logtime FROM _dash_stats where logtime >= DATE_SUB(NOW(), INTERVAL 2 HOUR)";
        $data = parent::query_obj("$sql");

        $lasthour = (int) str_replace(',','',$data[0]->total_servers);
        $curhour = (int) str_replace(',','',$data[1]->total_servers);

        $servers_lasthour = $curhour - $lasthour;


        $current_hour                   = $curhour;
        $current_hour_timestamp         = $data[0]->logtime;
        $last_hour                      = $lasthour;
        $last_hour_timestamp            = $data[1]->logtime;
        $counted_last_hour_servers      =  $servers_lasthour;


        $sql = "INSERT INTO `_servers_by_hour` (`current_servers`, `last_hour_servers`, `servers_hour`, `time_logged`)
        VALUES (
        \"$current_hour\",
        \"$last_hour\",
        \"$counted_last_hour_servers\",
        \"$current_hour_timestamp\");";

        if(parent::query_proc("$sql")){
            $msgtxt = "[$current_hour_timestamp] CURRENT HOUR servers:". $data[0]->total_servers ."\n
          [$last_hour_timestamp] LAST HOUR servers " .$data[1]->total_servers."\n
          Total servers last hour: $counted_last_hour_servers\n ";
        }

        return($msgtxt);

    }

}




