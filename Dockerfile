FROM php:7.1-fpm
MAINTAINER David St John <davestj@gmail.com>
EXPOSE 80

ENV CORELOCAL_VERSION 1.1.1

RUN echo -e -n "Percona MySQL Setup\n" \
    sudo yum -y install http://www.percona.com/downloads/percona-release/redhat/0.1-4/percona-release-0.1-4.noarch.rpm \
    sudo yum -y install Percona-XtraDB-Cluster-full-56.x86_64 percona-xtrabackup percona-toolkit pmm-client