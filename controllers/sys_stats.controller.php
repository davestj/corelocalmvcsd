<?php  
          /**     
           * CoreLocalMVCSD FrameWork
           * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
           *  
           * Class sys_stats
           * 
           */ 
           
          class __sys_stats {
              /** 
               * use trait GeneralConfig 
               */ 
              use GeneralConfig; 
              /** 
               * use trait DBConfig 
               */ 
              use DBConfig;

              /**
               * use trait ProdClusterDBConfig
               */
              use ProdClusterDBConfig;
           
              /** 
               * @var 
               */ 
              private $modelObj; 
           
              /** 
               * @var 
               */ 
              public $dateset; 
           
              /** 
               * @var Logger 
               */ 
              private $logobj; 
           
              /** 
               * @var 
               */ 
              public $sessionObj; 
           
              /** 
               * @var 
               */ 
              public $api_token;

              /**
               * @var string
               */
              public $set_api_token;





              public function __construct(){ 
                  $this->modelObj             = new sys_statsModel(self::thedsn("mysql"),self::theuser(),self::thepass());
                  //$this->sessionObj           = new DB_Session(self::Sessionconnect(), self::salty());
                  $this->sessionObj           = self::startSession();
                  $this->logobj               = new Logger(); 
                  $this->dateset              = date('F j, Y, g:i a');
                  $this->set_api_token        = self::ProdClusterApiToken();

              } 
           
              /** 
               * @return page default 
               */ 
              public function __default(){
                  header("Content-Type: application/json");
                  $this->api_token = $_REQUEST["apitoken"];
                  if($this->api_token == $this->set_api_token) {

                      $total_stats = array(
                          "AuthStatus"          => "TOKEN_AUTH_SUCCESS",
                          "TotalSystems"        => $this->modelObj->get_total_systems(),
                          "TotalDepartments"    => $this->modelObj->get_total_departments(),
                          "TotalFacilities"     => $this->modelObj->get_total_facilities(),
                          "TotalRounds"         => $this->modelObj->get_total_rounds(),
                          "TotalAnswers"        => $this->modelObj->get_total_answers(),
                          "TotalIssues"         => $this->modelObj->get_total_issues(),
                          "TotalIUsers"         => $this->modelObj->get_total_users(),
                          "TotalVisits"         => $this->modelObj->get_total_visits(),
                          "TotalPatients"       => $this->modelObj->get_total_patients()
                          );
                      print(json_encode($total_stats,JSON_HEX_QUOT));
                  }else{
                      $errormsg = array("Auth Status"  => "TOKEN_AUTH_FAILURE");
                      print(json_encode($errormsg,JSON_HEX_QUOT));
                  }
              } 
           
           
          } 
     
    

