<?php  
          /**     
           * CoreLocalMVCSD FrameWork
           * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
           *  
           * Class nagios_api  
           * 
           */ 
           
          class __nagios_api { 
              /** 
               * use trait GeneralConfig 
               */ 
              use GeneralConfig; 
              /** 
               * use trait DBConfig 
               */ 
              use DBConfig; 
           
              /** 
               * @var 
               */ 
              private $modelObj; 
           
              /** 
               * @var 
               */ 
              public $dateset; 
           
              /** 
               * @var Logger 
               */ 
              private $logobj; 
           
              /** 
               * @var 
               */ 
              public $sessionObj; 
           
              /** 
               * @var 
               */ 
              public $api_token; 
           
              public function __construct(){ 
                  $this->modelObj             = new nagios_apiModel(self::thedsn("mysql"),self::theuser(),self::thepass());
                  //$this->sessionObj           = new DB_Session(self::Sessionconnect(), self::salty());
                  $this->sessionObj           = self::startSession();
                  $this->logobj               = new Logger(); 
                  $this->dateset              = date('F j, Y, g:i a'); 
                  $this->api_token            = self::getSessionVar("API_TOKEN_CHECK"); 
              } 
           
              /** 
               * @return page default 
               */ 
              public function __default(){
                  header("Content-Type: application/json");
                echo '{ "api": "nagios", "version": "1.0" }';
              }

              /**
               * @return int
               */
              public function __nagios_global_stats(){
                  header("Content-Type: application/json");
                  $nagiosObj   = $this->modelObj->nagios_api_global_stats();
                  return(print($nagiosObj));
              }

              /**
               * @return int
               */
              public function __nagios_hosts_stats(){
                  header("Content-Type: application/json");
                  $nagh = $this->modelObj->nagios_api_host_stats();
                  return(print(json_encode($nagh,JSON_PRETTY_PRINT,512)));
              }

              /**
               * @return int
               */
              public function __nagios_services_stats(){
                  header("Content-Type: application/json");
                  $nags = $this->modelObj->nagios_api_service_stats();
                  return(print(json_encode($nags,JSON_PRETTY_PRINT,512)));
              }
           
           
          } 
     
    

