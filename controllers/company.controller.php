<?php  
      /**     
       * CoreLocalMVCSD FrameWork
       * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
       *  
       * Class company  
       * 
       */ 
       
      class __company  extends SmartyView implements PageStruct { 
          /** 
           * use trait GeneralConfig 
           */ 
          use GeneralConfig; 
          /** 
           * use trait DBConfig 
           */ 
          use DBConfig; 
          /** 
           * @var string 
           */ 
          public $viewpath = ''; 
          /** 
           * @var 
           */ 
          public $smarty; 
       
          /** 
           * @var 
           */ 
          private $dbObj; 
       
          /** 
           * @var 
           */ 
          public $dateset; 
       
          /** 
           * @var Logger 
           */ 
          private $logobj; 
       
          /** 
           * @var 
           */ 
          public $sessionObj; 
       
          /** 
           * @var 
           */ 
          public $login_check;

          /**
           * @var
           */
          public $logged_user;
       
          /** 
           * @param string $viewp 
           * @param null $cache 
           * @param null $debug 
           */ 
          public function __construct($viewp,$cache,$debug){ 
              parent::__construct($viewp, $cache, $debug); 
              $this->dbObj                = new companyModel(self::thedsn("mysql"),self::theuser(),self::thepass());
              //$this->sessionObj           = new DB_Session(self::Sessionconnect(), self::salty());
              $this->sessionObj           = self::startSession();
              $this->logobj               = new Logger(); 
              $this->viewpath             = $viewp;
              $this->cache                = $_REQUEST['cache'];
              $this->debugging            = $_REQUEST['debug'];
              $this->dateset              = date('F j, Y, g:i a'); 
              $this->assign("dateset",$this->dateset);
              $this->assign("theme",self::themeName());
              $this->login_check          = self::getSessionVar("LOGIN_CHECK");
              $this->logged_user          = self::getSessionVar("LOGGED_IN_USER");
              $this->assign("logged_in_user",$this->logged_user );
          } 
       
          /** 
           * @return page default 
           */ 
          public function __default(){ 
              if ($this->login_check != "OK"){ 
                  header("location: /login/"); 
              }else{
                  $this->assign("view_path", "/company/");
                  $this->global_header();
                  $this->display('company.tpl'); 
                  $this->global_footer(); 
              } 
          }

          /**
           * @return page assets
           */
          public function __assets(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->assign("view_path", "/company/");
                  $this->global_header();

                  $this->assign("assets", $this->dbObj->readAssets());

                  $this->display('assets.tpl');
                  $this->global_footer();
              }
          }

          /**
           * @return page add_new_asset
           */
          public function __add_new_asset(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->dbObj->createAsset($_REQUEST);
              }
          }

          /**
           * @return page edit_asset
           */
          public function __edit_asset(){
              if ($this->login_check != "OK"){
              header("location: /login/");
          }else{
                $this->dbObj->updateAsset($_REQUEST);
            }
          }


          /**
           * @return page delete_asset
           */
          public function __delete_asset(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->dbObj->deleteAsset($_REQUEST["device_id"]);
              }
          }


          /**
           * @return page vendors
           */
          public function __vendors(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->assign("view_path", "/company/");
                  $this->global_header();
                  $this->assign("vendors", $this->dbObj->readVendors());
                  //$this->display("header.tpl");
                  $this->display('vendors.tpl');
                  //$this->display("footer.tpl");
                  $this->global_footer();
              }
          }

          /**
           * @returnb page add_new_vendor
           */
          public function __add_new_vendor(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->dbObj->createVendor($_REQUEST);
              }
          }

          /**
           * @return page edit_vendor
           */
          public function __edit_vendor(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->dbObj->updateVendor($_REQUEST);
              }
          }

          /**
           * @return page delete_vendor
           */
          public function __delete_vendor(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->dbObj->deleteVendor($_REQUEST["vendor_id"]);
              }
          }

          /**
           * @return page office
           */
          public function __office(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->assign("view_path", "/company/");
                  $this->global_header();
                  $this->display('office.tpl');
                  $this->global_footer();
              }
          }

          /**
           * @return page finance
           */
          public function __finance(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->assign("view_path", "/company/");
                  $this->global_header();
                  $this->display('finance.tpl');
                  $this->global_footer();
              }
          }


          /** 
           * @return error page 
           * @param $code 
           */ 
          public function __error($code,$msg){ 
              $this->assign("error_code","$code"); 
              $this->assign("msg","$msg"); 
              $this->display("errors/$code.tpl"); 
          } 
       
       
      } 
 


