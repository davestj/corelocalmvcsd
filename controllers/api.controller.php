<?php
/**
 * CoreLocalMVCSD FrameWork
 * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
 *
 * Class __api
 *
 */


class __api  implements ApiStruct {
    /**
     * use trait GeneralConfig
     */
    use GeneralConfig;
    /**
     * use trait Dbconfig
     */
    use DBConfig;

    /**
     * Trait RedisConfig
     */
    use RedisConfig;
    /**
     * @var
     */
    private $modelObj;

    /**
     * @var
     */
    public $dateset;

    /**
     * @var Logger
     */
    private $logobj;

    /**
     * @var
     */
    public $sessionObj;

    /**
     * @var Redis
     */
    public $redis;

    /**
     * @param string $viewp
     * @param null $cache
     * @param null $debug
     */
    public function __construct($viewp,$cache,$debug){
        $this->modelObj             = new ApiModel(self::thedsn("mysql"),self::theuser(),self::thepass());
        $this->redis                = new Redis();
        $this->dateset              = date('F j, Y, g:i a');
        //$this->sessionObj           = new DB_Session(self::Sessionconnect(), self::salty());
        $this->sessionObj           = self::startSession();
        $this->logobj               = new Logger();
        $this->login_check          = self::getSessionVar("LOGIN_CHECK");

    }

    /**
     * @return page default
     */
    public function __default(){
        echo '{ "api": "config", "version": "1.0" }';
    }


    /**
     * @return api
     */
    public function __api(){
        header("Content-type: application/json");
        $apidata = array(
            "api_version" => "1",
            "php_version"       => phpversion(),
            "nginx_version"     => $_SERVER['SERVER_SOFTWARE'],
            "server_name"       => $_SERVER['SERVER_NAME'],
            "server_address"    => $_SERVER['SERVER_ADDR'],
            "app_environment"   => $_SERVER['APP_ENVIRONMENT_TYPE'],
            "date_stamp"        => $this->dateset);
        echo json_encode($apidata,JSON_PRETTY_PRINT);
    }

    /**
     *
     */
    public function __setup_complete(){
        header("Content-type: application/json");
        $setupdata = array(
            "setup_completed" => "OK",
            "php_version"       => phpversion(),
            "nginx_version"     => $_SERVER['SERVER_SOFTWARE'],
            "server_name"       => $_SERVER['SERVER_NAME'],
            "server_address"    => $_SERVER['SERVER_ADDR'],
            "app_environment"   => $_SERVER['APP_ENVIRONMENT_TYPE'],
            "date_setup"        => $this->dateset);
            echo json_encode($setupdata,JSON_PRETTY_PRINT);
    }

    /**
     *
     */
    public function __global_stats(){
        header("Content-type: application/json");
        $data   = $this->modelObj->global_stats();
        print(json_encode($data));
    }

    /**
     *
     */
    public function __session_data(){
        header("Content-type: application/json");
        $this->redis->connect('localhost', 6379);
        $this->redis->auth(self::redis_secretpass());
        $allKeys = array("All_Session_Data" => $this->redis->keys('*'));
        $sess = "PHPREDIS_SESSION:".self::getSessionID();
        $data = array("active_session_data" => $this->redis->get("$sess"));
        print(json_encode(array_merge($allKeys,$data),JSON_PRETTY_PRINT));

    }


    /**
     *
     */
    public function __trello(){
        //header("Content-Type: application/json");
        //$trello = $this->modelObj->get_all_trello_boards();
        $trello = $this->modelObj->get_all_trello_boards_preauth();

        echo "trello api";
        print_r($trello);
    }

    /**
     * @return page phpinfo();
     */
    public function __Pinfo(){
        return(phpinfo());
    }

    /**
     * @return error page
     * @param $code
     */
    public function __error($code,$msg){
        echo "$code - $msg";
    }



}


?>