<?php  
          /**     
           * CoreLocalMVCSD FrameWork
           * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
           *  
           * Class trello_api  
           * 
           */ 
           
          class __trello_api { 
              /** 
               * use trait GeneralConfig 
               */ 
              use GeneralConfig; 
              /** 
               * use trait DBConfig 
               */ 
              use DBConfig; 
           
              /** 
               * @var 
               */ 
              private $modelObj; 
           
              /** 
               * @var 
               */ 
              public $dateset; 
           
              /** 
               * @var Logger 
               */ 
              private $logobj; 
           
              /** 
               * @var 
               */ 
              public $sessionObj; 
           
              /** 
               * @var 
               */ 
              public $api_token; 
           
              public function __construct(){ 
                  $this->modelObj             = new trello_apiModel(self::thedsn("mysql"),self::theuser(),self::thepass());
                  //$this->sessionObj           = new DB_Session(self::Sessionconnect(), self::salty());
                  $this->sessionObj           = self::startSession();
                  $this->logobj               = new Logger();
                  $this->dateset              = date('F j, Y, g:i a'); 
                  $this->api_token            = self::getSessionVar("API_TOKEN_CHECK"); 
              } 
           
              /** 
               * @return page default 
               */
              public function __default(){
                  header("Content-Type: application/json");
                  echo '{ "api": "trello", "version": "1.0" }';
              }
           
           
          } 
     
    

