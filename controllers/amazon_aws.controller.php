<?php  
      /**     
       * CoreLocalMVCSD FrameWork
       * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
       *  
       * Class amazon_aws  
       * 
       */
use GuzzleHttp\Client;
use Guzzle\Stream\PhpStreamRequestFactory;

      class __amazon_aws  extends SmartyView implements PageStruct { 
          /** 
           * use trait GeneralConfig 
           */ 
          use GeneralConfig; 
          /** 
           * use trait DBConfig 
           */ 
          use DBConfig; 
          /** 
           * @var string 
           */ 
          public $viewpath = ''; 
          /** 
           * @var 
           */ 
          public $smarty; 
       
          /** 
           * @var 
           */ 
          private $dbObj; 
       
          /** 
           * @var 
           */ 
          public $dateset; 
       
          /** 
           * @var Logger 
           */ 
          private $logobj; 
       
          /** 
           * @var 
           */ 
          public $sessionObj; 
       
          /** 
           * @var 
           */ 
          public $login_check;

          /**
           * @var
           */
          public $logged_user;

          /** 
           * @param string $viewp 
           * @param null $cache 
           * @param null $debug 
           */ 
          public function __construct($viewp,$cache,$debug){ 
              parent::__construct($viewp, $cache, $debug); 
              $this->dbObj                = new amazon_awsModel(self::thedsn("mysql"),self::theuser(),self::thepass());
              //$this->sessionObj           = new DB_Session(self::Sessionconnect(), self::salty());
              $this->sessionObj           = self::startSession();
              $this->logobj               = new Logger(); 
              $this->viewpath             = $viewp;
              $this->cache                = $_REQUEST['cache'];
              $this->debugging            = $_REQUEST['debug'];
              $this->dateset              = date('F j, Y, g:i a');
              $this->login_check          = self::getSessionVar("LOGIN_CHECK");
              $this->logged_user          = self::getSessionVar("LOGGED_IN_USER");
              $this->assign("logged_in_user",$this->logged_user );
              $this->assign("dateset",$this->dateset);
              $this->assign("theme",self::themeName());

          } 
       
          /** 
           * @return page default 
           */ 
          public function __default(){ 
              if ($this->login_check != "OK"){ 
                  header("location: /login/"); 
              }else{ 
                  $this->assign("view_path", "/amazon_aws"); 
                  $this->global_header();
                  $json    = json_decode($this->dbObj->get_ec2_instances(),true,512);
                  $this->assign("ec2_intances", $json);
                  $this->display('amazon_aws.tpl'); 
                  $this->global_footer(); 
              } 
          }

          /**
           * @return page default
           */
          public function __aws_ec2_details(){
              $instanceID = $_REQUEST['id'];
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->assign("view_path", "/amazon_aws");
                  $this->global_header();
                  $json = json_decode($this->dbObj->get_ec2_instance("$instanceID"),true,512);
                  $grpName = $json[0]["SecurityGroup"];
                  $fwjson = json_decode($this->dbObj->get_ec2_instance_firewall_rules($grpName),true,512);
                  $this->assign("fwrules",$fwjson);
                  $this->assign("ec2_intance", $json);
                  $this->display('amazon_aws_ec2_details.tpl');
                  $this->global_footer();
              }
          }

          /** 
           * @return error page 
           * @param $code 
           */ 
          public function __error($code,$msg){ 
              $this->assign("error_code","$code"); 
              $this->assign("msg","$msg"); 
              $this->display("errors/$code.tpl"); 
          } 
       
       
      } 
 


