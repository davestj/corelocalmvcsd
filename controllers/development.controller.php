<?php  
      /**     
       * CoreLocalMVCSD FrameWork
       * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
       *  
       * Class development  
       * 
       */ 
       
      class __development  extends SmartyView implements PageStruct { 
          /** 
           * use trait GeneralConfig 
           */ 
          use GeneralConfig; 
          /** 
           * use trait DBConfig 
           */ 
          use DBConfig; 
          /** 
           * @var string 
           */ 
          public $viewpath = ''; 
          /** 
           * @var 
           */ 
          public $smarty;
          /** 
           * @var 
           */ 
          private $dbObj; 
       
          /** 
           * @var 
           */ 
          public $dateset;
          /** 
           * @var Logger 
           */ 
          private $logobj;
          /** 
           * @var 
           */ 
          public $sessionObj;
          /** 
           * @var 
           */ 
          public $login_check;
          /**
           * @var
           */
          public $logged_user;
       
          /** 
           * @param string $viewp 
           * @param null $cache 
           * @param null $debug 
           */ 
          public function __construct($viewp,$cache,$debug){ 
              parent::__construct($viewp, $cache, $debug); 
              $this->dbObj                = new developmentModel(self::thedsn("mysql"),self::theuser(),self::thepass());
              //$this->sessionObj           = new DB_Session(self::Sessionconnect(), self::salty());
              $this->sessionObj           = self::startSession();
              $this->logobj               = new Logger();
              $this->viewpath             = $viewp;
              $this->cache                = $_REQUEST['cache'];
              $this->debugging            = $_REQUEST['debug'];
              $this->dateset              = date('F j, Y, g:i a'); 
              $this->assign("dateset",$this->dateset);
              $this->assign("theme",self::themeName());
              $this->login_check          = self::getSessionVar("LOGIN_CHECK");
              $this->logged_user          = self::getSessionVar("LOGGED_IN_USER");
              $this->assign("logged_in_user",$this->logged_user );
          } 
       
          /** 
           * @return page default 
           */ 
          public function __default(){ 
              if ($this->login_check != "OK"){ 
                  header("location: /login/"); 
              }else{ 
                  $this->assign("view_path", "/development/");
                  $this->global_header(); 
                  $this->display('development.tpl'); 
                  $this->global_footer(); 
              } 
          }

          /**
           * @return page scm
           */
          public function __scm(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->assign("view_path", "/development/");
                  $this->global_header();
                  $this->display('development.tpl');
                  $this->global_footer();
              }
          }

          /**
           * @return page staging
           */
          public function __staging(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->assign("view_path", "/development/");
                  $this->global_header();
                  $this->display('development.tpl');
                  $this->global_footer();
              }
          }


          /**
           * @return page applications
           */
          public function __applications(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->assign("view_path", "/development/");
                  $this->global_header();
                  $this->display('applications.tpl');
                  $this->global_footer();
              }
          }



          /**
       * @return page apis
       */
          public function __apis(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->assign("view_path", "/development/");
                  $this->global_header();
                  $this->display('apis.tpl');
                  $this->global_footer();
              }
          }




          /**
           * @return page testing
           */
          public function __testing(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->assign("view_path", "/development/");
                  $this->global_header();
                  $this->display('testing.tpl');
                  $this->global_footer();
              }
          }



          /** 
           * @return error page 
           * @param $code 
           */ 
          public function __error($code,$msg){ 
              $this->assign("error_code","$code"); 
              $this->assign("msg","$msg"); 
              $this->display("errors/$code.tpl"); 
          } 
       
       
      } 
 


