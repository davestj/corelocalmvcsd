<?php  
          /**     
           * CoreLocalMVCSD FrameWork
           * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
           *  
           * Class aws_ec2_api  
           * 
           */ 
           
          class __aws_ec2_api { 
              /** 
               * use trait GeneralConfig 
               */ 
              use GeneralConfig; 
              /** 
               * use trait DBConfig 
               */ 
              use DBConfig; 
           
              /** 
               * @var 
               */ 
              private $modelObj; 
           
              /** 
               * @var 
               */ 
              public $dateset; 
           
              /** 
               * @var Logger 
               */ 
              private $logobj; 
           
              /** 
               * @var 
               */ 
              public $sessionObj; 
           
              /** 
               * @var 
               */ 
              public $api_token; 
           
              public function __construct(){ 
                  $this->modelObj             = new aws_ec2_apiModel(self::thedsn("mysql"),self::theuser(),self::thepass()); 
                  $this->sessionObj           = new DB_Session(self::Sessionconnect(), self::salty()); 
                  $this->logobj               = new Logger(); 
                  $this->dateset              = date('F j, Y, g:i a'); 
                  $this->api_token            = self::getSessionVar("API_TOKEN_CHECK"); 
              } 
           
              /** 
               * @return page default 
               */
              public function __default(){
                  header("Content-Type: application/json");
                  echo '{ "api": "aws-ec2", "version": "1.0" }';
              }
              /**
               * @return json array
               */
              public function __aws(){
               //   header("Content-Type: application/json");
               //     print_r($this->modelObj->get_aws_instances());
                  return(print (json_encode($this->modelObj->get_aws_reservations(),JSON_PRETTY_PRINT,512 )) );
              }

              /**
               * @return string
               */
              public function __aws_ec2_details(){
                  header("Content-Type: application/json");
                  $insID = $_REQUEST["instanceID"];
                  //print_r($this->dbObj->get_aws_reservation_details($insID));
                  return(print (json_encode($this->modelObj->get_aws_reservation_details($insID),JSON_PRETTY_PRINT,512 )) );

              }

              public function __aws_ec2_securitygroup_details(){
                  header("Content-Type: application/json");
                  $GrpName = $_REQUEST["groupName"];
                  //print_r($this->modelObj->get_aws_security_group_details($GrpName));
                  return(print (json_encode($this->modelObj->get_aws_security_group_details($GrpName),JSON_PRETTY_PRINT,512 )) );
              }
           
           
          } 
     
    

