<?php
/**
 * CoreLocalMVCSD FrameWork
 * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
 *
 * Class __auth
 *
 */



class __auth {
    /**
     * use trait GeneralConfig
     */
    use GeneralConfig;
    /**
     * use trait Dbconfig
     */
    use DBConfig;

    /**
     * use Trait GoogleAPIConfig
     */
    use GoogleAPIConfig;

    /**
     * use Trait ActiveDirectoryConfig
     */
    use ActiveDirectoryConfig;

    /**
     * @var
     */
    public $dbObj;

    /**
     * @var
     */
    public $dateset;

    /**
     * @var Logger
     */
    public $logobj;

    /**
     * @var
     */
    public $sessionObj;

    /**
     * @var AppAuth
     */
    public $DOlogin;

    /**
     * @var
     */
    public $username;
    /**
     * @var
     */
    public $password;
    /**
     * @param string $viewp
     * @param null $cache
     * @param null $debug
     */
    public function __construct($viewp,$cache,$debug){
        $this->dbObj                = new AuthModel(self::thedsn("mysql"),self::theuser(),self::thepass());
        //$this->sessionObj           = new DB_Session(self::Sessionconnect(), self::salty());
        $this->sessionObj           = self::startSession();
        $this->logobj       	    = new Logger();
        $this->dateset              = date('F j, Y, g:i a');
        $this->cache                = $_REQUEST['cache'];
        $this->debugging            = $_REQUEST['debug'];

    }

    /**
     * @return page default
     */
    public function __default(){
        echo "auth api";
    }


    /**
     * @param $params
     * @return bool
     */
    public function __authenticate($params){

        $this->username             = $params["UserName"];
        $this->password             = md5($params["Password"]);
        $this->DOlogin = $this->dbObj->db_auth_user("$this->username","$this->password");


        if(!empty($this->username) && !empty($this->password)){
            if($this->DOlogin) {
                self::setSessionVar("LOGIN_CHECK","OK");
                self::setSessionVar("LOGGED_IN_USER",$this->username);
                $this->dbObj->update_logintime($this->username);
                echo "OK";
                $this->logobj->logit("User  $this->username authenticated");
                return (TRUE);
            }else {
                self::setSessionVar("LOGIN_CHECK","AUTH_FAIL");
                echo "AUTH_FAIL";
                $this->logobj->logit("User failed to log in with: $this->username:$this->password");
                return (FALSE);
            }
        }else{
            self::setSessionVar("LOGIN_CHECK","EMPTY");
            echo "EMPTY";
            $this->logobj->logit("Empty login set attempted.");
            return(NULL);
        }


    }

    /**
     * @param $params
     * @return bool
     * @throws \Adldap\Exceptions\AdldapException
     * @throws \Adldap\Exceptions\ConfigurationException
     */
    public function __active_directory_auth($params){
        $this->username             = $params["UserName"];
        $this->password             = $params["Password"];

        $config = new \Adldap\Connections\Configuration();

        $config->setAccountSuffix(self::DomainSuffix());
        $config->setDomainControllers([self::DomainController()]);
        $config->setPort(389);
        $config->setBaseDn(self::BaseDSN());
        $config->setAdminUsername(self::BindDomainAdminUser());
        $config->setAdminPassword(self::BindDomainAdminPassW());
        $config->setFollowReferrals(false);
        $config->setUseSSL(false);
        $config->setUseTLS(true);
        $config->setUseSSO(false);

        $ad = new \Adldap\Adldap($config);


        if(!empty($this->username) && !empty($this->password)) {
            if ($ad->authenticate($this->username, $this->password)) {

                self::setSessionVar("LOGIN_CHECK", "OK");
                self::setSessionVar("LOGGED_IN_USER",$this->username);
                $this->dbObj->update_logintime($this->username);
                $this->logobj->logit("User  $this->username authenticated");
                echo "OK";
                return (TRUE);

            } else {
                self::setSessionVar("LOGIN_CHECK", "AUTH_FAIL");
                $this->logobj->logit("User  $this->username unable to authenticate");
                echo "AUTH_FAIL";
                return (FALSE);
            }
        }else{
            $this->logobj->logit("Empty login set attempted.");
            echo "EMPTY";
            return (FALSE);
        }


    }

    /**
     * @param $params
     */
    public function __google_auth($params){

        print_r(self::opauth_config());
        $Opauth = new Opauth( self::opauth_config() );


    }

    public function __oauth2callback($params){
        print_r($_REQUEST);
    }

    /**
     * @return page login
     */
    public function security_check($params){
        echo "sanitize login";
    }

    /**
     * @return page phpinfo();
     */
    public function __Pinfo(){
        return(phpinfo());
    }

    /**
     * @return error page
     * @param $code
     */
    public function __error($code,$msg){
        echo "$code - $msg";
    }



}


