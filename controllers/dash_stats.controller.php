<?php  
      /**     
       * CoreLocalMVCSD FrameWork
       * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
       *  
       * Class dash_stats  
       * 
       */ 
       
      class __dash_stats  extends SmartyView implements PageStruct { 
          /** 
           * use trait GeneralConfig 
           */ 
          use GeneralConfig; 
          /** 
           * use trait DBConfig 
           */ 
          use DBConfig; 
          /** 
           * @var string 
           */ 
          public $viewpath = ''; 
          /** 
           * @var 
           */ 
          public $smarty; 
       
          /** 
           * @var 
           */ 
          private $dbObj; 
       
          /** 
           * @var 
           */ 
          public $dateset; 
       
          /** 
           * @var Logger 
           */ 
          private $logobj; 
       
          /** 
           * @var 
           */ 
          public $sessionObj; 
       
          /** 
           * @var 
           */ 
          public $login_check;

          /**
           * @var
           */
          public $logged_user;
       
          /** 
           * @param string $viewp 
           * @param null $cache 
           * @param null $debug 
           */ 
          public function __construct($viewp,$cache,$debug){ 
              parent::__construct($viewp, $cache, $debug); 
              $this->dbObj                = new dash_statsModel(self::thedsn("mysql"),self::theuser(),self::thepass());
              //$this->sessionObj           = new DB_Session(self::Sessionconnect(), self::salty());
              $this->sessionObj           = self::startSession();
              $this->logobj               = new Logger(); 
              $this->viewpath             = $viewp;
              $this->cache                = $_REQUEST['cache'];
              $this->debugging            = $_REQUEST['debug'];
              $this->dateset              = date('F j, Y, g:i a'); 
              $this->assign("dateset",$this->dateset);
              $this->assign("theme",self::themeName());
              $this->login_check          = self::getSessionVar("LOGIN_CHECK");
              $this->logged_user          = self::getSessionVar("LOGGED_IN_USER");
              $this->assign("logged_in_user",$this->logged_user );
              $statsdata                  = $this->dbObj->get_stats();
              $this->assign("stats_data",$statsdata);
              $this->assign("view_path", "/dash_stats");
              $refry                = $_REQUEST["refresh"];
              $this->assign("refresh_state", $refry);
          } 
       
          /** 
           * @return page default 
           */ 
          public function __default(){


              $last24hour_assets    = $this->dbObj->get_last_24hour_assets();
              $last24hour_servers   = $this->dbObj->get_last_24hour_servers();
              $last24hour_systems   = $this->dbObj->get_last_24hour_systems();
              $last_24              = $this->dbObj->get_last_24hours();
              $this->assign("24hour_assets", $last24hour_assets);
              $this->assign("24hour_servers", $last24hour_servers);
              $this->assign("24hour_systems", $last24hour_systems);
              $this->assign("last_24hours", $last_24);
              $this->display('dash_stats.tpl');
          }

          public function __stats_assets(){

              $last24hour_assets    = $this->dbObj->get_last_24hour_assets();
              $this->assign("24hour_assets", $last24hour_assets);
              $this->display('assets_stats.tpl');


          }

          public function __stats_servers(){
              $last24hour_servers   = $this->dbObj->get_last_24hour_servers();
              $this->assign("24hour_servers", $last24hour_servers);
              $this->display('servers_stats.tpl');

          }

          public function __stats_systems(){
              $last24hour_systems   = $this->dbObj->get_last_24hour_systems();
              $this->assign("24hour_systems", $last24hour_systems);
              $this->display('systems_stats.tpl');


          }
       
       
          /** 
           * @return error page 
           * @param $code 
           */ 
          public function __error($code,$msg){ 
              $this->assign("error_code","$code"); 
              $this->assign("msg","$msg"); 
              $this->display("errors/$code.tpl"); 
          } 
       
       
      } 
 


