<?php
/**
 * CoreLocalMVCSD FrameWork
 * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
 *
 * Class __index
 *
 */

class __index  extends SmartyView implements PageStruct {
    /**
     * use trait GeneralConfig
     */
    use GeneralConfig;
    /**
     * use trait Dbconfig
     */
    use DBConfig;
    /**
     * @var string
     */
    public $viewpath = '';
    /**
     * @var
     */
    public $smarty;

    /**
     * @var
     */
    private $dbObj;

    /**
     * @var
     */
    public $dateset;

    /**
     * @var Logger
     */
    private $logobj;

    /**
     * @var
     */
    public $sessionObj;

    /**
     * @var MemcacheSession
     */
    public $memcacheObj;

    /**
     * @var
     */
    public $login_check;

    /**
     * @var
     */
    public $logged_user;

    /**
     * @param string $viewp
     * @param null $cache
     * @param null $debug
     */
public function __construct($viewp,$cache,$debug){
    parent::__construct($viewp, $cache, $debug);

    $this->dbObj                = new IndexModel(self::thedsn("mysql"),self::theuser(),self::thepass());
    //$this->sessionObj           = new DB_Session(self::Sessionconnect(), self::salty());
    $this->sessionObj           = self::startSession();
    $this->logobj       	    = new Logger();
    $this->viewpath             = $viewp;
    $this->cache                = $_REQUEST['cache'];
    $this->debugging            = $_REQUEST['debug'];
    $this->dateset              = date('F j, Y, g:i a');
    $this->login_check          = self::getSessionVar("LOGIN_CHECK");
    $this->logged_user          = self::getSessionVar("LOGGED_IN_USER");
    $this->assign("logged_in_user",$this->logged_user );
    $this->assign("dateset",$this->dateset);
    $this->assign("theme",self::themeName());
    $this->assign("session_settings",self::getSessionStatus());
    $this->assign("session_data",$this->sessionObj);


}

    /**
     * @return page default
     */
public function __default()
{


    if ($this->login_check != "OK"){
        header("location: /login/");
    }else{
        $this->assign("view_path", "/");
        $this->global_header();
        $this->display('index.tpl');
        $this->global_footer();
    }
}

    /**
     * @return page default
     */
    public function __dashboard(){

        if ($this->login_check != "OK"){
            header("location: /login/");
        }else{
            $this->global_header();
            $this->display('dashboard.tpl');
            $this->global_footer();
        }

    }

    /**
     * @return about us page
     */
    public function __about(){

        if ($this->login_check != "OK"){
            header("location: /login/");
        }else{
            $this->assign("view_path", "/about/");
            $this->global_header();
            $this->display('about.tpl');
            $this->global_footer();
        }


    }


    /**
     * @return page phpinfo();
     */
    public function __info(){
        if ($this->login_check != "OK"){
            header("location: /login/");
        }else{
            phpinfo();
        }

    }

    public function __logout(){
        self::destroySession();
        header("location: /login/");
    }

    /**
     * @return error page
     * @param $code
     */
    public function __error($code,$msg){
        $this->assign("error_code","$code");
        $this->assign("msg","$msg");
        $this->display("errors/$code.tpl");
    }



}


?>