<?php  
      /**     
       * CoreLocalMVCSD FrameWork
       * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
       *  
       * Class releases  
       * 
       */ 
       
      class __releases  extends SmartyView implements PageStruct { 
          /** 
           * use trait GeneralConfig 
           */ 
          use GeneralConfig; 
          /** 
           * use trait DBConfig 
           */ 
          use DBConfig; 
          /** 
           * @var string 
           */ 
          public $viewpath = ''; 
          /** 
           * @var 
           */ 
          public $smarty; 
       
          /** 
           * @var 
           */ 
          private $dbObj; 
       
          /** 
           * @var 
           */ 
          public $dateset; 
       
          /** 
           * @var Logger 
           */ 
          private $logobj; 
       
          /** 
           * @var 
           */ 
          public $sessionObj; 
       
          /** 
           * @var 
           */ 
          public $login_check; 
       
           /** 
            * @var 
            */ 
           public $logged_user; 
            
          /** 
           * @param string $viewp 
           * @param null $cache 
           * @param null $debug 
           */ 
          public function __construct($viewp,$cache,$debug){ 
              parent::__construct($viewp, $cache, $debug); 
              $this->dbObj                = new releasesModel(self::thedsn("mysql"),self::theuser(),self::thepass()); 
              $this->sessionObj           = self::startSession(); 
              $this->logobj               = new Logger(); 
              $this->viewpath             = $viewp;
              $this->cache                = $_REQUEST['cache'];
              $this->debugging            = $_REQUEST['debug'];
              $this->dateset              = date('F j, Y, g:i a'); 
              $this->assign("dateset",$this->dateset); 
              $this->assign("theme",self::themeName()); 
              $this->login_check          = self::getSessionVar("LOGIN_CHECK"); 
              $this->logged_user          = self::getSessionVar("LOGGED_IN_USER"); 
              $this->assign("logged_in_user",$this->logged_user ); 
          } 
       
          /** 
           * @return page default 
           */ 
          public function __default(){ 
              if ($this->login_check != "OK"){ 
                  header("location: /login/"); 
              }else{ 
                  $this->assign("view_path", "/releases"); 
                  $this->global_header(); 
                  $this->display('releases.tpl'); 
                  $this->global_footer(); 
              } 
          }

          /**
           * @return page dashboard
           */
          public function __dashboard(){
              if ($this->login_check != "OK"){
                  header("location: /login/");
              }else{
                  $this->assign("view_path", "/releases");
                  $this->global_header();
                  $this->display('releases.tpl');
                  $this->global_footer();
              }
          }

          /** 
           * @return error page 
           * @param $code 
           */ 
          public function __error($code,$msg){ 
              $this->assign("error_code","$code"); 
              $this->assign("msg","$msg"); 
              $this->display("errors/$code.tpl"); 
          } 
       
       
      } 
 

