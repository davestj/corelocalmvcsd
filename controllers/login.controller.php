<?php
/**
 * CoreLocalMVCSD FrameWork
 * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
 *
 * Class __login
 *
 */

class __login  extends SmartyView implements PageStruct {
    /**
     * use trait GeneralConfig
     */
    use GeneralConfig;
    /**
     * use trait Dbconfig
     */
    use DBConfig;
    /**
     * @var string
     */
    public $viewpath = '';
    /**
     * @var
     */
    public $smarty;

    /**
     * @var
     */
    private $dbObj;

    /**
     * @var
     */
    public $dateset;

    /**
     * @var Logger
     */
    private $logobj;

    /**
     * @var DB_Session
     */
    public $sessionObj;

    /**
     * @var MemcacheSession
     */
    public $memcacheObj;

    /**
     * @var $login_check
     */
    public $login_check;
    /**
     * @param string $viewp
     * @param null $cache
     * @param null $debug
     */

    /**
     * @var
     */
    public $envi_type;

    public function __construct($viewp,$cache,$debug){
        parent::__construct($viewp, $cache, $debug);

        $this->dbObj                = new LoginModel(self::thedsn("mysql"),self::theuser(),self::thepass());
        //$this->sessionObj           = new DB_Session(self::Sessionconnect(), self::salty());
        $this->sessionObj           = self::startSession();
        $this->logobj       	    = new Logger();
        $this->viewpath             = $viewp;
        $this->cache                = $_REQUEST['cache'];
        $this->debugging            = $_REQUEST['debug'];
        $this->dateset              = date('F j, Y, g:i a');
        $this->envi_type            = self::get_env();
        $this->assign("env_type",$this->envi_type);
        $this->assign("dateset",$this->dateset);
        $this->assign("theme",self::themeName());
        $this->login_check          = self::getSessionVar("LOGIN_CHECK");

    }

    /**
     * @return page default
     */
    public function __default(){
        if ($this->login_check != "OK") {
            $this->display('login.tpl');
        }else{
            header("location: /?page=dashboard");
        }


    }



    /**
     * @return error page
     * @param $code
     */
    public function __error($code,$msg){
        $this->assign("error_code","$code");
        $this->assign("msg","$msg");
        $this->display("errors/$code.tpl");
    }



}


?>