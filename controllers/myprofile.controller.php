<?php  
      /**     
       * CoreLocalMVCSD FrameWork
       * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
       *  
       * Class myprofile  
       * 
       */ 
       
      class __myprofile  extends SmartyView implements PageStruct { 
          /** 
           * use trait GeneralConfig 
           */ 
          use GeneralConfig; 
          /** 
           * use trait DBConfig 
           */ 
          use DBConfig; 
          /** 
           * @var string 
           */ 
          public $viewpath = ''; 
          /** 
           * @var 
           */ 
          public $smarty; 
       
          /** 
           * @var 
           */ 
          private $dbObj; 
       
          /** 
           * @var 
           */ 
          public $dateset; 
       
          /** 
           * @var Logger 
           */ 
          private $logobj; 
       
          /** 
           * @var 
           */ 
          public $sessionObj; 
       
          /** 
           * @var 
           */ 
          public $login_check;

          /**
           * @var
           */
          public $logged_user;
       
          /** 
           * @param string $viewp 
           * @param null $cache 
           * @param null $debug 
           */ 
          public function __construct($viewp,$cache,$debug){ 
              parent::__construct($viewp, $cache, $debug); 
              $this->dbObj                = new myprofileModel(self::thedsn("mysql"),self::theuser(),self::thepass());
              //$this->sessionObj           = new DB_Session(self::Sessionconnect(), self::salty());
              $this->sessionObj           = self::startSession();
              $this->logobj               = new Logger(); 
              $this->viewpath             = $viewp;
              $this->cache                = $_REQUEST['cache'];
              $this->debugging            = $_REQUEST['debug'];
              $this->dateset              = date('F j, Y, g:i a'); 
              $this->assign("dateset",$this->dateset);
              $this->assign("theme",self::themeName());
              $this->login_check          = self::getSessionVar("LOGIN_CHECK");
              $this->logged_user          = self::getSessionVar("LOGGED_IN_USER");
              $this->assign("logged_in_user",$this->logged_user );
          } 
       
          /** 
           * @return page default 
           */ 
          public function __default(){ 
              if ($this->login_check != "OK"){ 
                  header("location: /login/"); 
              }else{ 
                  $this->assign("view_path", "/myprofile"); 
                  $this->global_header();
                  if(self::get_env() == "LOCAL"){
                      $ud = $this->dbObj->get_local_user_account($this->logged_user);
                      print_r($ud);
                  }else {
                      $ud = $this->dbObj->get_ad_account($this->logged_user);
                      print_r($ud);
                      $ad_attribute = $ud["attributes"];
                      $fullname = $ad_attribute["cn"];
                      //print_r($fullname);
                      //$this->display('myprofile.tpl');
                      echo "Full name: $fullname[0]<br>";
                  }
                  $this->global_footer();
              } 
          } 
       
       
          /** 
           * @return error page 
           * @param $code 
           */ 
          public function __error($code,$msg){ 
              $this->assign("error_code","$code"); 
              $this->assign("msg","$msg"); 
              $this->display("errors/$code.tpl"); 
          } 
       
       
      } 
 


