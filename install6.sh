#!/bin/bash

echo -e -n "Installer for core.local\n"
sudo rm -rvf /root/.bash_profile && sudo rm -rfv /root/.bashrc && \
sudo cp -v /vagrant/setup/bash_profile /root/.bash_profile && sudo cp -v /vagrant/setup/bashrc /root/.bashrc && \
sudo rm -rvf /home/vagrant/.bash_profile && sudo rm -rfv /home/vagrant/.bashrc && \
sudo cp -v /vagrant/setup/bash_profile /home/vagrant/.bash_profile && sudo cp -v /vagrant/setup/bashrc /home/vagrant/.bashrc && \
sudo chown -v vagrant.vagrant  /home/vagrant/.bash_profile && sudo chown -v vagrant.vagrant  /home/vagrant/.bashrc

#setup directories
sudo mkdir -vp /var/www/core.local/logs

#setup and install mysql first
sudo yum -y install Percona-XtraDB-Cluster-full-56.x86_64 percona-xtrabackup percona-toolkit pmm-client

#setup and install php and nginx
sudo yum -y install vim mailx mlocate libmemcached10.x86_64 libmemcached10-devel.x86_64 memcached.x86_64 redis32u.x86_64 sysstat telnet ftp dos2unix php71u-bcmath php71u-cli.x86_64 php71u-common.x86_64 php71u-dba.x86_64 php71u-dbg.x86_64 php71u-devel.x86_64 php71u-embedded.x86_64 php71u-enchant.x86_64 php71u-fpm.x86_64 php71u-fpm-nginx.noarch php71u-gd.x86_64 php71u-gmp.x86_64 php71u-imap.x86_64 php71u-interbase.x86_64 php71u-intl.x86_64 php71u-json.x86_64 php71u-ldap.x86_64 php71u-mbstring.x86_64 php71u-mcrypt.x86_64 php71u-mysqlnd.x86_64 php71u-odbc.x86_64  php71u-pdo.x86_64 php71u-pdo-dblib.x86_64 php71u-pecl-apcu.x86_64 php71u-pecl-apcu-devel.x86_64 php71u-pecl-apcu-panel.noarch php71u-pecl-igbinary.x86_64 php71u-pecl-igbinary-devel.x86_64 php71u-pecl-redis.x86_64 php71u-pecl-xdebug.x86_64 php71u-pgsql.x86_64 php71u-process.x86_64 php71u-pspell.x86_64 php71u-recode.x86_64 php71u-snmp.x86_64 php71u-soap.x86_64 php71u-tidy.x86_64 php71u-xml.x86_64 php71u-xmlrpc.x86_64

#Setup network
sudo echo -e -n "192.168.88.20\t\tcore.local\n" | sudo tee --append /etc/hosts;
sudo sed -i 's/HOSTNAME=localhost.localdomain/HOSTNAME=core.local/g' /etc/sysconfig/network;

#install chef
sudo curl -LO https://omnitruck.chef.io/install.sh && sudo bash ./install.sh -v 12.13.30 && sudo rm install.sh

#mysql setup
#mysql_config_editor set --login-path=core_local --host=core.local --user=root --password
#mysql_config_editor set --login-path=core_localhost --host=localhost --user=root --password

sudo cp -v /vagrant/setup/mysql-server/mylogin.cnf /root/.mylogin.cnf && \
sudo cp -v /vagrant/setup/mysql-server/mylogin.cnf /home/vagrant/.mylogin.cnf;
sudo chown -v vagrant.vagrant /home/vagrant/.mylogin.cnf && sudo chmod -v 0600 /home/vagrant/.mylogin.cnf && sudo chmod -v 0600 /root/.mylogin.cnf

cd /vagrant/; sudo rm -rfv bin/ && sudo mkdir -v bin && cd bin && sudo php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
sudo php -r "if (hash_file('SHA384', 'composer-setup.php') === '55d6ead61b29c7bdee5cccfb50076874187bd9f21f65d8991d46ec5cc90518f447387fb9f76ebae1fbbacf329e583e30') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;";
sudo php /vagrant/bin/composer-setup.php;
sudo php -r "unlink('/vagrant/bin/composer-setup.php');" && sudo mv -v composer.phar composer && sudo chmod -v 755 composer;