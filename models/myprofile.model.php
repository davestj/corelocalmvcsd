<?php 
      /** 
       * CoreLocalMVCSD FrameWork
       * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
       * 
       * myprofileModel 
       * 
       * 
       * Class myprofile 
       * Extends MasterDb 
       */ 
           
      class myprofileModel  extends MasterDb{ 
          use DBConfig; 
          use GeneralConfig;
          use ActiveDirectoryConfig;

          /**
           * @var
           */
          public $username;

          /**
           * @var
           */
          public $password;

          /**
           * @var string
           */
          private $error = '';

          /**
           * @var
           */
          public $adconfig;

          /**
           * @var
           */
          public $sql;

          public function __construct($dsn, $user = "", $passwd = ""){ 
              $options = array( 
                  PDO::ATTR_PERSISTENT => true, 
                  PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION 
              ); 
      
              try { 
                  parent::__construct($dsn, $user, $passwd, $options); 
              } catch (PDOException $e) { 
                  $this->error = $e->getMessage(); 
              } 
          }

          /**
           * @return \Adldap\Connections\Configuration
           * @throws \Adldap\Exceptions\ConfigurationException
           */
          public function set_ad_config(){
              $this->adconfig = new \Adldap\Connections\Configuration();
              $this->adconfig->setAccountSuffix(self::DomainSuffix());
              $this->adconfig->setDomainControllers([self::DomainController()]);
              $this->adconfig->setPort(389);
              $this->adconfig->setBaseDn(self::BaseDSN());
              $this->adconfig->setAdminUsername(self::BindDomainAdminUser());
              $this->adconfig->setAdminPassword(self::BindDomainAdminPassW());
              $this->adconfig->setFollowReferrals(false);
              $this->adconfig->setUseSSL(false);
              $this->adconfig->setUseTLS(true);
              $this->adconfig->setUseSSO(false);

              return($this->adconfig);
          }

          /**
           * @param $ad_username
           * @return \Adldap\Models\User|bool
           */
          public function get_ad_account($ad_username){
              $this->username = "$ad_username";
              $ad = new \Adldap\Adldap(self::set_ad_config());
              $userD = $ad->users()->find($this->username);
              return($userD);
          }

          public function  get_local_user_account($username){
              $this->sql = parent::query_single("SELECT * FROM `_users`  WHERE user_name = '$username'");


              $data[] = array(
                  "id"       => $this->sql["id"],
                  "fname"    => $this->sql["fname"],
                  "lname"    => $this->sql["lname"],
                  "email"    => $this->sql["user_email"],
                  "usertype" => $this->sql["user_type"]
                  );

             // print_r($ret);
              return($data);

          }


          /** 
           * @return array 
           */ 
          public function show_db_status(){ 
              $status = parent::query_all("SHOW STATUS"); 
              return($status); 
          } 
      
      
      }


