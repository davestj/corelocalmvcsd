<?php 
      /** 
       * CoreLocalMVCSD FrameWork
       * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
       * 
       * dash_statsModel 
       * 
       * 
       * Class dash_stats 
       * Extends MasterDb 
       */ 
           
      class dash_statsModel  extends MasterDb{ 
          use DBConfig; 
          use GeneralConfig;
          use ProdClusterDBConfig;
              /**  
               * @var string  
               */  
              private $error = '';
          /**
           * @var
           */
                public $json;
          /**
           * @var
           */
                public $xml;
          /**
           * @var
           */
          public $statsObj;

          /**
           * @param $dsn
           * @param string $user
           * @param string $passwd
           */
          public function __construct($dsn, $user = "", $passwd = ""){ 
              $options = array( 
                  PDO::ATTR_PERSISTENT => true, 
                  PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION 
              ); 
      
              try {
                  parent::__construct($dsn, $user, $passwd, $options); 
              } catch (PDOException $e) { 
                  $this->error = $e->getMessage(); 
              } 
          }

          /**
           * @return mixed
           */
          public function stats_api(){
              $client = new GuzzleHttp\Client(['verify' => false]);
              $url = "https://".self::syshost()."/api/?page=global_stats";
              $request = $client->get("$url");
              $this->json = json_decode($request->getBody());
              return($this->json);
          }

          /**
           * @return array
           */
          public function get_stats(){
              $this->statsObj = self::stats_api();


              $g_stat = array(
                  "AuthStatus"          => $this->statsObj->AuthStatus,
                  "TotalSystems"        => $this->statsObj->TotalSystems,
                  "TotalAssets"         => $this->statsObj->Totalassets,
                  "TotalServers"        => $this->statsObj->Totalservers,
                  "TotalIUsers"         => $this->statsObj->TotalIUsers
                  );

              return($g_stat);

          }

          /**
           * @return array
           */
          public function get_last_24hour_assets(){
              $sql = "SELECT * FROM _assets_by_hour where time_logged >= DATE_SUB(NOW(),INTERVAL 24 HOUR);";
              $data = parent::query_obj("$sql");

              foreach($data as $hdata){

                  $dataArr[] = array(
                      "AssetID"             => $hdata->id,
                      "CurrentAssetCount"   => $hdata->current_assets,
                      "LastHourAssetCount"  => $hdata->last_hour_assets,
                      "AssetsLastHour"      => $hdata->assets_hour,
                      "TimeLogged"          => $hdata->time_logged
                  );
              }
              return($dataArr);
          }

          /**
           * @return array
           */
          public function get_last_24hour_servers(){
              $sql = "SELECT * FROM _servers_by_hour where time_logged >= DATE_SUB(NOW(),INTERVAL 24 HOUR);";
              $data = parent::query_obj("$sql");

              foreach($data as $hdata){

                  $dataArr[] = array(
                      "ServersID"             => $hdata->id,
                      "CurrentServersCount"   => $hdata->current_servers,
                      "LastHourServersCount"  => $hdata->last_hour_servers,
                      "ServersLastHour"       => $hdata->servers_hour,
                      "TimeLogged"            => $hdata->time_logged
                  );
              }
              return($dataArr);
          }

          /**
           * @return array
           */
          public function get_last_24hour_systems(){
              $sql = "SELECT * FROM _systems_by_hour where time_logged >= DATE_SUB(NOW(),INTERVAL 24 HOUR);";
              $data = parent::query_obj("$sql");

              foreach($data as $hdata){

                  $dataArr[] = array(
                      "SystemsID"             => $hdata->id,
                      "CurrentSystemsCount"   => $hdata->current_systems,
                      "LastHourSystemsCount"  => $hdata->last_hour_systems,
                      "SystemsLastHour"       => $hdata->systems_hour,
                      "TimeLogged"            => $hdata->time_logged
                  );
              }
              return($dataArr);
          }


          /**
           * @return array
           */
          public function get_last_24hours(){
              $assetsArr        = self::get_last_24hour_assets();
              $serversArr       = self::get_last_24hour_servers();
              $systemsArr        = self::get_last_24hour_systems();
              $last_24hours     = array_merge($assetsArr,$serversArr,$systemsArr);
              return($last_24hours);
          }
      
      }


