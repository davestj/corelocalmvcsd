<?php
/**
 * CoreLocalMVCSD FrameWork
 * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
 *
 * developmentModel
 *
 *
 * Class development
 * Extends MasterDb
 */

class developmentModel  extends MasterDb{
    use DBConfig;
    use GeneralConfig;

    /**
     * @var string
     */
    public $q = "";
    /**
     * @var Logger
     */
    public $logobj;
    /**
     * @var string
     */
    private $error = '';


    public function __construct($dsn, $user = "", $passwd = ""){
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        try {
            parent::__construct($dsn, $user, $passwd, $options);
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
        }
        $this->logobj       	    = new Logger();
    }

    /**
     * @return array
     */
    public function show_db_status(){
        $status = parent::query_all("SHOW STATUS");
        $this->logobj->logit($status);
        return($status);
    }


    /**
     * @param $servername
     * @param $serverip
     * @return bool
     */
    public function add_vpn_server($servername,$serverip){
        $checkdupe = (bool) self::check_existing_vpn_server($serverip);

        if($checkdupe){
            //insert vpn server to db
            $createVpnServer = self::insert_vpn_server($servername,$serverip);
            if($createVpnServer){
                return(TRUE);
            }else{
                return(FALSE);
            }
        }else{
            return(FALSE);
        }

    }

    /**
     * @param $name
     * @param $ip
     * @return bool
     * @throws Exception
     */
    public function insert_vpn_server($name,$ip){
        //insert vpn server to db
        $qObj = self::prepare("INSERT INTO _vpn_servers(dns_name,server_ip) VALUES(:ServerName, :ServerIp)");

        try{
            $qObj->execute(array(
                "ServerName"              => $name,
                "ServerIp"                => $ip
            ));
            $this->logobj->logit("INSERTED NEW VPN SERVER");
            return(TRUE);

        }catch(PDOException $p){
            $msg = "Caught exception:".$p->getMessage() ."\ntrace :". $p->getTrace() ." ";
            $this->logobj->logit($msg);
            return(FALSE);
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get_vpn_server($id){
        $qObj = self::query_single("SELECT * FROM _vpn_servers WHERE id = '$id'");
        return($qObj);

    }

    /**
     * @return array
     */
    public function get_vpn_servers(){
        $qObj = self::query_all("SELECT * FROM _vpn_servers");
        return($qObj);
    }


    /**
     * @param $formdata
     * @return string
     */
    public function add_vpn_config($formdata){
        $insert = self::insert_vpn_config($formdata);
        return($insert);
    }


    /**
     * @param $params
     * @return string
     * @throws Exception
     */
    public function insert_vpn_config($params){
        $cfgname                 = $params["cfg_name"];
        $salifetime              = $params["salifetime"];
        $srvid                   = $params["server_id"];
        $conntype                = $params["connection_type"];
        $leftid                  = $params["left_id"];
        $leftsubnets             = $params["left_subnets"];
        $leftsourceip            = $params["left_sourceip"];
        $rightid                 = $params["right"];
        $rightsubnets            = $params["right_subnets"];
        $rightsourceip           = $params["right_sourceip"];
        $auth_by                 = $params["authby"];
        $ike                     = $params["ike"];
        $ike_dhg                 = $params["ike_dhg"];
        $phase2alg               = $params["phase2alg"];
        $phase2alg_dhg           = $params["phase2alg_dhg"];
        $pfs                     = $params["pfs"];
        $forceencaps             = $params["forceencaps"];

        $sqlObj = self::prepare("INSERT INTO _vpn_configs(server_id,config_name,connection_type,authby,left_id,left_subnets,left_sourceip,right_id,right_subnets,right_sourceip,pfs,ike,phase2alg,
salifetime,aggrmode,forceencaps,date_added) VALUES(:ServerID,:ConfigName,:ConnType,:AuthBy,:LeftID,:LeftSubNets,:LeftSourceIP,:RightID,:RightSubNets,:RightSourceIP,:Pfs,:Ike,:Phase2Alg,:Salifetime,:AggrMode,
:ForceEncaps,NOW())");


        try{
            $sqlObj->execute(array(
                "ServerID"          => $srvid,
                "ConfigName"        => $cfgname,
                "ConnType"          => $conntype,
                "AuthBy"            => $auth_by,
                "LeftID"            => $leftid,
                "LeftSubNets"       => $leftsubnets,
                "LeftSourceIP"      => $leftsourceip,
                "RightID"           => $rightid,
                "RightSubNets"      => $rightsubnets,
                "RightSourceIP"     => $rightsourceip,
                "Pfs"               => $pfs,
                "Ike"               => "$ike;$ike_dhg",
                "Phase2Alg"         => "$phase2alg;$phase2alg_dhg",
                "Salifetime"        => $salifetime,
                "AggrMode"          => "No",
                "ForceEncaps"       => $forceencaps
            ));

            $this->logobj->logit("INSERTED NEW VPN CONFIG");
            $msg = array('statusmsg' => 'OK', 'message' => 'Added new VPN Config');
            return(json_encode($msg));

        }catch(PDOException $p){
            $msg = "Caught exception:".$p->getMessage() ."\ntrace :". $p->getTrace() ." ";
            $this->logobj->logit($msg);
            $msg2 = array('statusmsg' => 'NACK', 'message' => 'Unable to add new VPN Config');
            return(json_encode($msg2));
        }

    }

    /**
     * @param $id
     * @return mixed
     */
    public function get_vpn_config($id){
        $qObj = self::query_single("SELECT * FROM _vpn_configs WHERE id = '$id'");
        return($qObj);

    }

    /**
     * @return array
     */
    public function get_vpn_configs(){
        $qObj = self::query_all("SELECT * FROM _vpn_configs");
        return($qObj);
    }

    /**
     * @param $serverip
     * @return bool
     * @throws Exception
     */
    public function check_existing_vpn_server($serverip){
        $serverip_check = self::query_single("SELECT id FROM _vpn_servers WHERE server_ip = '$serverip'");
        $sID            = (int) $serverip_check["id"];

        if($sID > 0){
            $this->logobj->logit("vpn server already exists");
            return(FALSE);
        }else{
            $this->logobj->logit("duplicate vpn server check ok, ready to add new vpn server.");
            return(TRUE);
        }

    }

}

