<?php
/**
 * CoreLocalMVCSD FrameWork
 * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
 *
 * Class LoginModel
 */

class LoginModel extends MasterDb{
    use DBConfig;
    use GeneralConfig;


    /**
     * @var string
     */
    private $error = '';

    public function __construct($dsn, $user = "", $passwd = ""){
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        try {
            parent::__construct($dsn, $user, $passwd, $options);
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
        }
    }

    /**  EXAMPLES
     * @return array
     */
    public function show_dbs(){
        $dbs = parent::query_all("SHOW DATABASES");
        return($dbs);
    }

    /**  EXAMPLES
     * @return array
     */
    public function show_db_vars(){
        $dbvars = parent::query_obj("SHOW VARIABLES");
        return($dbvars);
    }
}
