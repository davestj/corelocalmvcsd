<?php 
          /** 
           * CoreLocalMVCSD FrameWork
           * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
           * 
           * aws_ec2_apiModel 
           * 
           * 
           * Class aws_ec2_api 
           * Extends MasterDb 
           */
use Aws\Ec2\Ec2Client;
use Aws\Common\Enum\Region;
use Aws\Credentials\Credentials;

          class aws_ec2_apiModel  extends MasterDb{ 
              use DBConfig; 
              use GeneralConfig;
              use AwsConfig;
          /**
            * @var
            */
            public $dateset;
            
            /**
            * @var string
            */
            public $xml    = '';
            
            /**
            * @var array
            */
            public $apidata = '';
            
          
           /**
           * @var array
           */
           public $error = '';
          
              public function __construct($dsn, $user = "", $passwd = ""){ 
                  $options = array( 
                      PDO::ATTR_PERSISTENT => true, 
                      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION 
                  ); 
          
                  try { 
                      parent::__construct($dsn, $user, $passwd, $options); 
                  } catch (PDOException $e) { 
                      $this->error = $e->getMessage(); 
                  } 
              }

              /**
               * @return static
               */
              public function auth_aws(){
                  $credentials = new Credentials(self::cfg_aws_key(), self::cfg_aws_secret());
                  $aws =  Aws\Ec2\Ec2Client::factory(array(
                      'credentials' => $credentials,
                      'version' => 'latest',
                      'region' => 'us-west-2'
                  ));
                  return($aws);

              }

              /**
               * @return mixed
               */
              public function get_aws_instances(){
                  $aws = self::auth_aws();
                  return($aws->DescribeInstances());
              }


              /**
               * @return array
               */
              public function get_aws_reservations()
              {
                  $instances = self::get_aws_instances();
                  $reservations = $instances['Reservations'];

                  foreach ($reservations as $reservation) {
                      $instanceobj = $reservation['Instances'];
                      foreach ($instanceobj as $instance) {
                          $instanceName = '';

                          //get each instance name
                          foreach ($instance['Tags'] as $tag) {
                              if ($tag['Key'] == 'Name') {
                                  $instanceName = $tag['Value'];
                              }
                          }

                          //get each instance network interface
                          foreach ($instance['NetworkInterfaces'] as $NetworkInterface) {
                              if ($NetworkInterface['Description'] == 'Primary network interface') {
                                  $mac    = $NetworkInterface['MacAddress'];
                                  $privip = $NetworkInterface['PrivateIpAddress'];
                                  $pubip  = $NetworkInterface['Association']['PublicIp'];
                                  $pubdns = $NetworkInterface['Association']['PublicDnsName'];
                              }
                          }
                          $data[] = array(
                              'InstanceName'     => $instanceName,
                              'State'            => $instance['State']['Name'],
                              'InstanceID'       => $instance['InstanceId'],
                              'ImageID'          => $instance['ImageId'],
                              'Arch'             => $instance['Architecture'],
                              'NetworkMAC'       => $mac,
                              'PrivateIP'        => $privip,
                              'PublicIP'         => $pubip,
                              'PublicDNS'        => $pubdns,
                              'PrivateDnsName'   => $instance['PrivateDnsName'],
                              'PublicDnsName'    => $instance['PublicDnsName'],
                              'InstanceType'     => $instance['InstanceType'],
                              'MonitoringState'  => $instance['Monitoring']['State'],
                              'SecurityGroup'    => $instance['SecurityGroups'][0]['GroupName']);



                      }

                  }
                  return($data);
              }

              /**
               * @param $groupName
               * @return mixed
               */
              public function get_aws_security_group_details($groupName){
                  $aws = self::auth_aws();
                  $secuirtyGroup = $aws->DescribeSecurityGroups(array(
                      'Filters' => array(
                          array('Name' => 'group-name', 'Values' => array(''.$groupName.'')),
                      )
                  ));

                  $secgroups = $secuirtyGroup['SecurityGroups'];
                  return($secgroups);
              }
              /**
               * @param $instanceID
               * @return array
               */
              public function get_aws_reservation_details($instanceID){
                  $aws = self::auth_aws();
                  $instances = $aws->DescribeInstances(array(
                      'Filters' => array(
                          array('Name' => 'instance-id', 'Values' => array(''.$instanceID.'')),
                      )
                  ));

                  $reservations = $instances['Reservations'];

                  foreach ($reservations as $reservation) {
                      $instanceobj = $reservation['Instances'];
                      foreach ($instanceobj as $instance) {
                          $instanceName = '';

                          //get each instance name
                          foreach ($instance['Tags'] as $tag) {
                              if ($tag['Key'] == 'Name') {
                                  $instanceName = $tag['Value'];
                              }
                          }

                          //get each instance network interface
                          foreach ($instance['NetworkInterfaces'] as $NetworkInterface) {
                              if ($NetworkInterface['Description'] == 'Primary network interface') {
                                  $mac    = $NetworkInterface['MacAddress'];
                                  $privip = $NetworkInterface['PrivateIpAddress'];
                                  $pubip  = $NetworkInterface['Association']['PublicIp'];
                                  $pubdns = $NetworkInterface['Association']['PublicDnsName'];
                              }
                          }

                          //get each block device
                          $d = 0;
                          foreach ($instance['BlockDeviceMappings'] as $BlockDeviceMapping) {
                              $d++;
                              $device   = $BlockDeviceMapping['DeviceName'];
                                $ebsdata  = $BlockDeviceMapping['Ebs'];
                                    $ebs_id             = $ebsdata['VolumeId'];
                                    $ebs_status         = $ebsdata['Status'];
                                    $ebs_attachtime     = $ebsdata['date'];
                                    $ebs_delete         = $ebsdata['DeleteOnTerminations'];

                                    $device_data[] = array(
                                        'DeviceCount'         => $d,
                                        'DeviceName'          => $device,
                                        'DeviceID'            => $ebs_id,
                                        'DeviceStatus'        => $ebs_status,
                                        'DeviceAttachStatus'  => $ebs_attachtime,
                                        'DevieTermination'    => $ebs_delete
                                    );

                          }

                          $data[] = array(
                              'InstanceName'     => $instanceName,
                              'State'            => $instance['State']['Name'],
                              'InstanceID'       => $instance['InstanceId'],
                              'ImageID'          => $instance['ImageId'],
                              'VirtType'         => $instance['VirtualizationType'],
                              'NetworkMAC'       => $mac,
                              'PrivateIP'        => $privip,
                              'PublicIP'         => $pubip,
                              'PublicDNS'        => $pubdns,
                              'Arch'             => $instance['Architecture'],
                              'RootDevType'      => $instance['RootDeviceType'],
                              'RootDevice'       => $instance['RootDeviceName'],
                              'DeviceData'       => $device_data,
                              'PrivateDnsName'   => $instance['PrivateDnsName'],
                              'PublicDnsName'    => $instance['PublicDnsName'],
                              'InstanceType'     => $instance['InstanceType'],
                              'MonitoringState'  => $instance['Monitoring']['State'],
                              'SecurityGroup'    => $instance['SecurityGroups'][0]['GroupName']);



                      }

                  }
                  return($data);
              }
          
              /** 
               * @return array 
               */ 
              public function show_db_status(){ 
                  $status = parent::query_all("SHOW STATUS"); 
                  return($status); 
              } 
          
          
          }
    

