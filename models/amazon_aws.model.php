<?php 
      /** 
       * CoreLocalMVCSD FrameWork
       * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
       * 
       * amazon_awsModel 
       * 
       * 
       * Class amazon_aws 
       * Extends MasterDb 
       */
use GuzzleHttp\Client;

      class amazon_awsModel  extends MasterDb{ 
          use DBConfig; 
          use GeneralConfig;

          /**
           * @var string
           */
          private $error = '';

          /**
           * @param $dsn
           * @param string $user
           * @param string $passwd
           */
          public function __construct($dsn, $user = "", $passwd = ""){ 
              $options = array( 
                  PDO::ATTR_PERSISTENT => true, 
                  PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION 
              ); 
      
              try { 
                  parent::__construct($dsn, $user, $passwd, $options); 
              } catch (PDOException $e) { 
                  $this->error = $e->getMessage(); 
              } 
          }

          /**
           * @param $url
           * @return \Psr\Http\Message\StreamInterface
           */
        public function RESTClient($url){

            $client = new GuzzleHttp\Client(['verify' => false]);
            $request = $client->get($url);
            $data    = $request->getBody();
                return($data);
        }
          /**
           * @return \Psr\Http\Message\ResponseInterface
           */
          public function get_ec2_instances(){
              $res = self::RESTClient("https://".self::syshost()."/aws_ec2_api/?page=aws");
              return ($res);
          }

          /**
           * @param $id
           * @return object
           */
          public function get_ec2_instance($id){
              $res = self::RESTClient("https://".self::syshost()."/aws_ec2_api/?page=aws_ec2_details&instanceID=$id");
              return ($res);
          }

          /**
           * @param $groupName
           * @return \Psr\Http\Message\StreamInterface
           */
          public function get_ec2_instance_firewall_rules($groupName){
              $res = self::RESTClient("https://".self::syshost()."/aws_ec2_api/?page=aws_ec2_securitygroup_details&groupName=$groupName");
              return ($res);

          }
      
          /** 
           * @return array 
           */ 
          public function show_db_status(){ 
              $status = parent::query_all("SHOW STATUS"); 
              return($status); 
          } 
      
      
      }


