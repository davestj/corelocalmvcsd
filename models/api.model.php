<?php
/**
 * CoreLocalMVCSD FrameWork
 * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
 *
 * Class ApiModel
 */
use Aws\Ec2\Ec2Client;
use Aws\Common\Enum\Region;
use Aws\Credentials\Credentials;

class ApiModel extends MasterDb{
    use DBConfig;
    use GeneralConfig;

    /**
     * @var
     */
    public $dateset;

    /**
     * @var string
     */
    public $xml    = '';

    /**
     * @var array
     */
    public $apidata =array();


    /**
     * @var string
     */
    private $error = '';

    /**
     * @var string
     */
    public $nagObj = '';

    public function __construct($dsn, $user = "", $passwd = ""){
        $this->dateset              = date('F j, Y, g:i a');
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        try {
            parent::__construct($dsn, $user, $passwd, $options);
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
        }
    }

    /**
     * @return array
     */
    public function count_systems(){
        $systemCount = parent::query_single("SELECT COUNT(id) FROM _systems");
        $this->apidata = ''.$systemCount["COUNT(id)"].'';
        return($this->apidata);
    }

    /**
     * @return array
     */
    public function count_assets(){
        $assetCount = parent::query_single("SELECT COUNT(id) FROM _assets");
        $this->apidata = ''.$assetCount["COUNT(id)"].'';
        return($this->apidata);
    }

    /**
     * @return array
     */
    public function global_stats(){
        $assets         = self::count_assets();
        $systems        = self::count_systems();
        $this->apidata  = array (
            "total_assets"  => $assets,
            "total_systems" => $systems
            );

        return($this->apidata);
}

    /**  EXAMPLES
     * @return array
     */
    public function show_db_vars(){
        $dbvars = parent::query_obj("SHOW VARIABLES");
        return($dbvars);
    }
}
