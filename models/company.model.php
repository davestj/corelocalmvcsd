<?php 
      /** 
       * CoreLocalMVCSD FrameWork
       * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
       * 
       * companyModel 
       * 
       * 
       * Class company 
       * Extends MasterDb 
       */ 
           
      class companyModel  extends MasterDb{ 
          use DBConfig; 
          use GeneralConfig;


          /**
           * @var string
           */
          private $error        = '';

          /**
           * @var array
           */
          public $sqlData       = array('var' => 'value');


          public $type_enum     = array();
          /**
           * @param $dsn
           * @param string $user
           * @param string $passwd
           */
          public function __construct($dsn, $user = "", $passwd = ""){
              $options = array(
                  PDO::ATTR_PERSISTENT => true,
                  PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
              );

              try {
                  parent::__construct($dsn, $user, $passwd, $options);
              } catch (PDOException $e) {
                  $this->error = $e->getMessage();
              }
          }

          /**
           * @param $form_data
           */
          public function createAsset($form_data){

              $device_type          = $form_data["device_type"];
              $device_name          = $form_data["device_name"];
              $purch_date           = $form_data["purch_date"];
              $serial_num           = $form_data["serial_num"];
              $loaned_to_email      = $form_data["loaned_to_email"];
              $loaned_to            = $form_data["loaned_to"];
              $issue_date           = $form_data["issue_date"];
              $current_condition    = $form_data["current_condition"];
              $mac_address          = $form_data["mac_address"];
              $asset_department     = $form_data["asset_department"];
              $provider             = $form_data["provider"];
              $item_value           = $form_data["item_value"];
              $tagid                = $form_data["tag_id"];


              try {
                  parent::query_proc('CALL core_add_asset("'.$device_type.'",
              "'.$device_name.'",
              "'.$purch_date.'",
              "'.$serial_num.'",
              "'.$loaned_to_email.'",
              "'.$loaned_to.'",
              "'.$issue_date.'",
              "'.$current_condition.'",
              "'.$mac_address.'",
              "'.$asset_department.'",
              "'.$provider.'",
              "'.$item_value.'",
              "'.$tagid.'"
              )');
                  echo '
            <div class="ui-widget">
                <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                    <strong>Added New Asset</strong><div id="result_set">'.$device_type.' <br>'.$device_name.' </div> </p>
                </div>
            </div>
            ';
              }catch(PDOException $ex) {
                  echo '
                  <div class="ui-widget">
	                <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
		                <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
		                <strong>Alert:</strong>
		                <div id="result_set">Error occured '.$ex->getMessage().'</div>
		                </p>
	                </div>
                </div>
                ';
              }



          }

          /**
           * @return array
           */
          public function readAssets(){
              $sqlD                 = parent::query_obj("SELECT * FROM _assets");
              //$this->type_enum      = parent::query_enumerated_values("_assets","device_type",TRUE);
              $this->type_enum      = array('Office','Laptop','Tablet','Media Hub','TV','Phone','Printer','Speaker','Monitor','Switch','Entertainment','Gateway','Network');
              foreach($sqlD as $sd) {
                  $sqlData[] = array(
                      'AssetID'           => ''.$sd->id.'',
                      'DeviceType'        => ''.$sd->device_type.'',
                      'DeviceTypes'       => ''.$this->type_enum.'',
                      'DeviceName'        => ''.$sd->device_name.'',
                      'PurchaseDate'      => ''.$sd->purchase_date.'',
                      'SerialNo'          => ''.$sd->serial_no.'',
                      'LoanedToEmail'     => ''.$sd->loan_to_email.'',
                      'LoanedTo'          => ''.$sd->loan_to.'',
                      'IssueDate'         => ''.$sd->issue_date.'',
                      'CurrentCondition'  => ''.$sd->current_condition.'',
                      'ReturnedOn'        => ''.$sd->returned_on.'',
                      'MACAddress'        => ''.$sd->mac_address.'',
                      'Department'        => ''.$sd->department.'',
                      'Provider'          => ''.$sd->provider.'',
                      'ItemValue'         => ''.$sd->item_value.'',
                      'AssetTag'          => ''.$sd->tag.''
                  );
              }

              return($sqlData);

          }



          /**
           * @param $form_data
           */
          public function updateAsset($form_data){
              $device_ID            = $form_data["device_id"];
              $device_type          = $form_data["device_type"];
              $device_name          = $form_data["device_name"];
              $purch_date           = $form_data["purch_date"];
              $serial_num           = $form_data["serial_num"];
              $loaned_to_email      = $form_data["loaned_to_email"];
              $loaned_to            = $form_data["loaned_to"];
              $issue_date           = $form_data["issue_date"];
              $current_condition    = $form_data["current_condition"];
              $mac_address          = $form_data["mac_address"];
              $asset_department     = $form_data["asset_department"];
              $provider             = $form_data["provider"];
              $item_value           = $form_data["item_value"];
              $tagid                = $form_data["tag_id"];

              try {
                  parent::query_proc('CALL core_update_asset(
              "'.$device_ID.'",
              "'.$device_type.'",
              "'.$device_name.'",
              "'.$purch_date.'",
              "'.$serial_num.'",
              "'.$loaned_to_email.'",
              "'.$loaned_to.'",
              "'.$issue_date.'",
              "'.$current_condition.'",
              "'.$mac_address.'",
              "'.$asset_department.'",
              "'.$provider.'",
              "'.$item_value.'",
              "'.$tagid.'"
              )');
                  echo '
                <div class="ui-widget">
                <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                    <strong>Edit Asset</strong>
                    <div id="edit_asset_resultset'.$device_ID.'">'.$device_type.' <br>'.$device_name.' </div>
                    </p>
                </div>
            </div>
                  ';
              }catch(PDOException $ex) {
                  echo '
                  <div class="ui-widget">
	                <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
		                <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
		                <strong>Alert:</strong>
		                <div id="edit_asset_resultset'.$device_ID.'">Error occured '.$ex->getMessage().'</div>
		                </p>
	                </div>
                </div>
                ';
              }
          }


          /**
           * @param $asset_id
           * @return bool
           */
          public function deleteAsset($asset_id){
              try {
                  parent::query_proc('CALL core_del_asset("'.$asset_id.'")');
                  echo '
                <div class="ui-widget">
                <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                    <strong>Deleting Asset</strong>
                    <div id="edit_asset_resultset'.$asset_id.'">removed asset id: '.$asset_id.' <br></div>
                    </p>
                </div>
            </div>
                  ';
              }catch(PDOException $ex) {
                  echo '
                  <div class="ui-widget">
	                <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
		                <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
		                <strong>Alert:</strong>
		                <div id="edit_asset_resultset'.$device_ID.'">Error occured while removing asset id '.$asset_id.'<br> '.$ex->getMessage().'</div>
		                </p>
	                </div>
                </div>
                ';
              }

          }


          /**
           * @return array
           */
          public function readVendors(){
              $sqlD = parent::query_obj("SELECT * FROM _vendors");


              foreach($sqlD as $sd) {
                  $sqlData[] = array(
                      'VendorID'             => ''.$sd->id.'',
                      'VendorName'           => ''.$sd->vendor_name.'',
                      'Category'             => ''.$sd->category.'',
                      'VendorType'           => ''.$sd->vendor_type.'',
                      'WWWURL'               => ''.$sd->website_url.'',
                      'VendorAccountID'      => ''.$sd->account_id.'',
                      'VendorAccountUser'    => ''.$sd->account_username.'',
                      'VendorAccountPass'    => ''.$sd->account_pass.'',
                      'VendorPhone'          => ''.$sd->phone_number.'',
                      'VendorEmail'          => ''.$sd->contact_email.'',
                      'VendorNotes'          => ''.$sd->account_notes.'',
                      'DateEdited'           => ''.$sd->date_edited.'',
                      'DateAdded'            => ''.$sd->date_added.'',


                  );
              }

              return($sqlData);

          }

          /**
           * @param $form_data
           */
          public function createVendor($form_data){

              $vname                = $form_data["vendor_name"];
              $vcat                 = $form_data["vendor_category"];
              $vtype                = $form_data["vendor_type"];
              $vwww                 = $form_data["www_url"];
              $vaccount_id          = $form_data["account_id"];
              $vaccount_uname       = $form_data["account_username"];
              $vaccount_pass        = $form_data["account_pass"];
              $vphone               = $form_data["phone_numer"];
              $vemai                = $form_data["contact_email"];
              $vnotes               = $form_data["account_notes"];

              try {
                  parent::query_proc('CALL core_add_vendor(
              "'.$vname.'",
              "'.$vcat.'",
              "'.$vtype.'",
              "'.$vwww.'",
              "'.$vaccount_id.'",
              "'.$vaccount_uname.'",
              "'.$vaccount_pass.'",
              "'.$vphone.'",
              "'.$vemai.'",
              "'.$vnotes.'"
              )');
                  echo '
            <div class="ui-widget">
                <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                    <strong>Added New Vendor</strong><div id="result_set">'.$vname.' <br>'.$vtype.' </div> </p>
                </div>
            </div>
            ';
              }catch(PDOException $ex) {
                  echo '
                  <div class="ui-widget">
	                <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
		                <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
		                <strong>Alert:</strong>
		                <div id="result_set">Error occured '.$ex->getMessage().'</div>
		                </p>
	                </div>
                </div>
                ';
              }

          }

          /**
           * @param $form_data
           */
          public function updateVendor($form_data){
              $vendor_ID            = $form_data["vendor_id"];
              $vendor_name          = $form_data["vendor_name"];
              $vendor_category      = $form_data["vendor_category"];
              $vendor_type          = $form_data["vendor_type"];
              $vendor_www           = $form_data["www_url"];
              $vendor_acct_id       = $form_data["account_id"];
              $vendor_acct_user     = $form_data["account_username"];
              $vendor_acct_pass     = $form_data["account_pass"];
              $vendor_phonenum      = $form_data["phone_number"];
              $vendor_email         = $form_data["vendor_email"];
              $vendor_notes         = $form_data["account_notes"];

              try {
                  parent::query_proc('CALL core_update_vendor(
              "'.$vendor_ID.'",
              "'.$vendor_name.'",
              "'.$vendor_category.'",
              "'.$vendor_type.'",
              "'.$vendor_www.'",
              "'.$vendor_acct_id.'",
              "'.$vendor_acct_user.'",
              "'.$vendor_acct_pass.'",
              "'.$vendor_phonenum.'",
              "'.$vendor_email.'",
              "'.$vendor_notes.'"
              )');
                  echo '
                <div class="ui-widget">
                <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                    <strong>Edit Asset</strong>
                    <div id="edit_vendor_resultset'.$vendor_ID.'">'.$vendor_type.' <br>'.$vendor_name.' </div>
                    </p>
                </div>
            </div>
                  ';
              }catch(PDOException $ex) {
                  echo '
                  <div class="ui-widget">
	                <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
		                <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
		                <strong>Alert:</strong>
		                <div id="edit_vendor_resultset'.$vendor_ID.'">Error occured '.$ex->getMessage().'</div>
		                </p>
	                </div>
                </div>
                ';
              }
          }




          public function deleteVendor($vendor_id){
              try {
                  parent::query_proc('CALL core_del_vendor("'.$vendor_id.'")');
                  echo '
                <div class="ui-widget">
                <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                    <strong>Deleting Vendor</strong>
                    <div id="delete_vendor_resultset'.$vendor_id.'">removed vendor id: '.$vendor_id.' <br></div>
                    </p>
                </div>
            </div>
                  ';
              }catch(PDOException $ex) {
                  echo '
                  <div class="ui-widget">
	                <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
		                <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
		                <strong>Alert:</strong>
		                <div id="delete_vendor_resultset'.$vendor_ID.'">Error occured while removing vendor id '.$vendor_id.'<br> '.$ex->getMessage().'</div>
		                </p>
	                </div>
                </div>
                ';
              }

          }
      
          /** 
           * @return array 
           */ 
          public function show_db_status(){ 
              $status = parent::query_all("SHOW STATUS"); 
              return($status); 
          } 
      
      
      }


