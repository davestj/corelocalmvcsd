<?php 
          /** 
           * CoreLocalMVCSD FrameWork
           * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
           * 
           * nagios_apiModel 
           * 
           * 
           * Class nagios_api 
           * Extends MasterDb 
           */ 
               
          class nagios_apiModel  extends MasterDb{ 
              use DBConfig; 
              use GeneralConfig;
              use NagiosConfig;
          /**
            * @var
            */
            public $dateset;
            
            /**
            * @var string
            */
            public $xml    = '';
            
            /**
            * @var array
            */
            public $apidata = '';
            
          
           /**
           * @var array
           */
           public $error = '';
          
              public function __construct($dsn, $user = "", $passwd = ""){ 
                  $options = array( 
                      PDO::ATTR_PERSISTENT => true, 
                      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION 
                  ); 
          
                  try { 
                      parent::__construct($dsn, $user, $passwd, $options); 
                  } catch (PDOException $e) { 
                      $this->error = $e->getMessage(); 
                  } 
              }

              /**
               * @return SimpleXMLElement|string
               */
              public function nagios_api(){
                  $client = new GuzzleHttp\Client(['verify' => false]);
                  $request = $client->get(self::cfg_nagius_api_url());
                  $this->xml = simplexml_load_string($request->getBody(),'SimpleXMLElement');
                  return($this->xml);
              }

              /**
               * @return array
               */
              public function nagios_api_host_stats(){
                  $this->nagObj = self::nagios_api();
                  $h_stat = $this->nagObj->hosts;
                  $h = 0;

                  foreach($h_stat->host as $hostObj){
                      $h++;
                  }
                  $this->apidata = '"total_monitored_hosts": '.$h.'';
                  return($this->apidata);
              }

              /**
               * @return array
               */
              public function nagios_api_service_stats(){
                  $this->nagObj = self::nagios_api();
                  $s_stat = $this->nagObj->services;
                  $s = 0;

                  foreach($s_stat->service as $srvcObj){
                      $s++;
                  }
                  $this->apidata = '"total_monitored_services": '.$s.'';
                  return($this->apidata);
              }

              /**
               * @return SimpleXMLElement|string
               */
              public function nagios_api_global_stats(){
                  $this->nagObj = self::nagios_api();
                  $gl = $this->nagObj->programStatus;

                  $this->apidata = '
                {
                "modified_host_attributes": '.$gl->modified_host_attributes.',
                "modified_service_attributes": '.$gl->modified_service_attributes.',
                '.self::nagios_api_host_stats().',
                '.self::nagios_api_service_stats().'
                }';


                  return($this->apidata);
              }
          
              /** 
               * @return array 
               */ 
              public function show_db_status(){ 
                  $status = parent::query_all("SHOW STATUS"); 
                  return($status); 
              } 
          
          
          }
    

