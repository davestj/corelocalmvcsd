<?php
/**
 * CoreLocalMVCSD FrameWork
 * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
 *
 * Class IndexModel
 */

class AuthModel extends MasterDb{
    use DBConfig;
    use GeneralConfig;



    /**
     * @var string
     */
    private $error = '';

    public function __construct($dsn, $user = "", $passwd = ""){
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        try {
            parent::__construct($dsn, $user, $passwd, $options);
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
        }
    }


    public function db_auth_user($username,$password){
        $auth = self::query_all("SELECT id FROM _users WHERE user_name = '$username' AND pass_word = '$password'");
        if($auth) {
            return (TRUE);
        }
    }

    public function update_logintime($username){
        self::query("UPDATE _users SET last_login = NOW() WHERE user_name = '$username'");
    }
}
