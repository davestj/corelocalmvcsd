<?php 
          /** 
           * CoreLocalMVCSD FrameWork
           * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
           * 
           * trello_apiModel 
           * 
           * 
           * Class trello_api 
           * Extends MasterDb 
           */ 
               
          class trello_apiModel  extends MasterDb{ 
              use DBConfig; 
              use GeneralConfig;
              use TrelloConfig;
          /**
            * @var
            */
            public $dateset;
            
            /**
            * @var string
            */
            public $xml    = '';
            
            /**
            * @var array
            */
            public $apidata = '';
            
          
           /**
           * @var array
           */
           public $error = '';
          
              public function __construct($dsn, $user = "", $passwd = ""){ 
                  $options = array( 
                      PDO::ATTR_PERSISTENT => true, 
                      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION 
                  ); 
          
                  try { 
                      parent::__construct($dsn, $user, $passwd, $options); 
                  } catch (PDOException $e) { 
                      $this->error = $e->getMessage(); 
                  } 
              }

              /**
               * @return \Trello\Trello
               */
              public function auth_new_trello(){
                  $client = new \Trello\Trello(self::cfg_trello_key(), self::cfg_trello_secret());
                  return($client);
              }

              /**
               * @return \Trello\Trello
               */
              public function auth_existing_trello(){
                  $trello = new \Trello\Trello(self::cfg_trello_key(), self::cfg_trello_secret(), self::cfg_trello_oauth_token(), self::cfg_trell_oauth_verifier());
                  return($trello);
              }

              public function get_all_trello_boards_preauth(){
                  $trello = self::auth_existing_trello();
                  return($trello->members->get('my/boards'));
              }
              /**
               * @return mixed
               */
              public function get_all_trello_boards(){
                  $trello = new \Trello\Trello(self::cfg_trello_key(), self::cfg_trello_secret());
                  $trello->authorize(array(
                      'expiration' => 'never',
                      'scope' => array(
                          'read' => true,
                          'write' => true
                      ),
                      'name' => 'core.local'
                  ));
                  return($trello->members->get('my/boards'));
              }

              /** 
               * @return array 
               */ 
              public function show_db_status(){ 
                  $status = parent::query_all("SHOW STATUS"); 
                  return($status); 
              } 
          
          
          }
    

