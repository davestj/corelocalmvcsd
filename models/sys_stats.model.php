<?php 
          /** 
           * CoreLocalMVCSD FrameWork
           * @copyright 2017
           * 
           * sys_statsModel
           * 
           * 
           * Class sys_stats
           * Extends MasterDb 
           */ 
               
          class sys_statsModel  extends MasterDb{
              /**
               * Use DBConfig trait for local db connection
               */
              use DBConfig;
              /**
               * Use GeneralConfig trait
               */
              use GeneralConfig;

          /**
            * @var
            */
            public $dateset;
            
            /**
            * @var string
            */
            public $xml    = '';
            
            /**
            * @var array
            */
            public $apidata = '';

              /**
               * @var
               */
              public $clusterObj;
          
           /**
           * @var array
           */
           public $error = '';
              /**
               * @var
               */
              public $sqlD;

              /**
               * @param $dsn
               * @param string $user
               * @param string $passwd
               */
              public function _construct($dsn, $user = "", $passwd = ""){
                  $options = array( 
                      PDO::ATTR_PERSISTENT => true, 
                      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION 
                  ); 
          
                  try { 
                      parent::__construct($dsn, $user, $passwd, $options);

                  } catch (PDOException $e) { 
                      $this->error = $e->getMessage(); 
                  }


              }


              /**
               * @return string
               */
              public function get_total_assets(){
                  $this->sqlD =  parent::query_single("SELECT count(id) as `total_assets` FROM _assets;");
                  $total = number_format($this->sqlD["total_assets"]);
                  return($total);
              }

              /**
               * @return string
               */
              public function get_total_systems(){
                  $this->sqlD =   parent::query_single("SELECT count(id) as `total_systems` FROM _systems;");
                  $total = number_format($this->sqlD["total_systems"]);
                  return($total);
              }

              /**
               * @return string
               */
              public function get_total_servers(){
                  $this->sqlD = parent::query_single("SELECT count(id) as `total_servers` FROM _servers;");
                  $total = number_format($this->sqlD["total_servers"]);
                  return($total);
              }



              /** 
               * @return array 
               */ 
              public function show_db_status(){ 
                  $status = parent::query_all("SHOW STATUS"); 
                  return($status); 
              } 
          
          
          }
    

