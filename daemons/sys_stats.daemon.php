#!/usr/bin/php -q
<?php
/**
 * CoreLocalMVCSD FrameWork
 * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
 *
 * Daemon sys_statsService
 *
 */
$servicepath = basename(__DIR__);
require_once( dirname(__FILE__).'/../config/config.common.php');
Load::model("sys_stats");
Load::service("sys_stats");

$modelObj             = new sys_statsService(CoreLocal::get_mysql_dbdsn(),CoreLocal::get_dbuser(),CoreLocal::get_dbpass());
$dateset              = date('F j, Y, g:i a');
$assets          = $modelObj->get_all_assets();
$systems          = $modelObj->get_all_systems();
$servers         = $modelObj->get_all_servers();



$updatestatus = $modelObj->update_dash_status("$assets","$systems","$servers");

if($updatestatus == "UPDATED"){
    echo "[$dateset] STATS UPDATE SUCCESS\n[$dateset] Total Assets: $assets | Total Servers: $servers | Total Systems: $systems\n";
}elseif($updatestatus == "FAILED"){
    echo "QUERY FAILED TO UPDATE";
}else{
    echo "NO DATA\n";
}
$modelObj->calculate_assets_by_hour();
$modelObj->calculate_servers_by_hour();
$modelObj->calculate_systems_by_hour();



