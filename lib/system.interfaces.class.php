<?php
/**
 * CoreLocalMVCSD FrameWork
 * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
 *
 * Interface PageStruct
 */
interface PageStruct{

    /**
     * @return mixed
     */
    public function __default();

    /**
     * @param $code
     * @return mixed
     */
    public function __error($code,$msg);

}


/**
 * Interface ApiStruct
 */
interface ApiStruct{
    /**
     * @return mixed
     */
    public function __default();
    
    public function __api();

    public function __error($code,$msg);

}


/**
 * Interface AuthStruct
 */
interface AuthStruct{

    /**
     * @return mixed
     */
    public function __default();

    /**
     * @return mixed
     */
    public function __authenticate($params);

    /**
     * @return mixed
     */
    public function security_check($params);
}


