<?php
/**
 * CoreLocalMVCSD FrameWork
 * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
 *
 * Class __CHANGEME_CONTROLLER
 *
 */

class __CHANGEME_CONTROLLER  extends SmartyView implements PageStruct {
    /**
     * use trait GeneralConfig
     */
    use GeneralConfig;
    /**
     * use trait DBConfig
     */
    use DBConfig;
    /**
     * @var string
     */
    public $viewpath = '';
    /**
     * @var
     */
    public $smarty;

    /**
     * @var
     */
    private $dbObj;

    /**
     * @var
     */
    public $dateset;

    /**
     * @var Logger
     */
    private $logobj;

    /**
     * @var
     */
    public $sessionObj;

    /**
     * @var
     */
    public $login_check;

    /**
     * @param string $viewp
     * @param null $cache
     * @param null $debug
     */
    public function __construct($viewp,$cache,$debug){
        parent::__construct($viewp, $cache, $debug);
        $this->dbObj                = new __CHANGEME_MODELl(self::thedsn("mysql"),self::theuser(),self::thepass());
        $this->sessionObj           = self::startSession();
        $this->logobj       	    = new Logger();
        $this->viewpath             = $viewp;
        $this->cache                = $cache;
        $this->debugging            = $debug;
        $this->dateset              = date('F j, Y, g:i a');
        $this->assign("dateset",$this->dateset);
        $this->assign("theme",self::themeName());
        $this->login_check          = self::getSessionVar("LOGIN_CHECK");
    }

    /**
     * @return page default
     */
    public function __default(){
        if ($this->login_check != "OK"){
            header("location: /login/");
        }else{
            $this->global_header();
            $this->display('__CHANGEME_VIEW__.tpl');
            $this->global_footer();
        }
    }


    /**
     * @return error page
     * @param $code
     */
    public function __error($code,$msg){
        $this->assign("error_code","$code");
        $this->assign("msg","$msg");
        $this->display("errors/$code.tpl");
    }



}
