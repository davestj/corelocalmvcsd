<?php
/**
 * CoreLocalMVCSD FrameWork
 * GPL 2.0 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
 *
 * CHANGEME_MODEL model
 *
 *
 * Class CHANGEME_MODEL
 * Extends MasterDb
 */

class CHANGEME_MODEL  extends MasterDb{
    use DBConfig;
    use GeneralConfig;

    /**
     * @var string
     */
    public $q = "";
    /**
     * @var Logger
     */
    public $logobj;
    /**
     * @var string
     */
    private $error = '';

    public function __construct($dsn, $user = "", $passwd = ""){
        $this->logobj       	    = new Logger();
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        try {
            parent::__construct($dsn, $user, $passwd, $options);
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
        }
    }

    /**
     * @return array
     */
    public function show_db_status(){
        $status = parent::query_all("SHOW STATUS");
        return($status);
    }


}
